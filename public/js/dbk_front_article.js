	var dbkItemNo = 0;
	var swiper = null;
	var dbkItem;
	$(document).ready(function(){
		// 파라미터를 통한 값 가져오기 sjh
		const urlParams = new URLSearchParams(window.location.search);
		const board_no = urlParams.get('board_no');
		let param = {};
		param.board_no = board_no;
		if(urlParams.has('category_no') === true) {
			const category_no = urlParams.get('category_no');
			param.category_no = category_no;
		}

		$.ajax({
			url: 'https://egongegong.kr/front/articles',
			type: 'GET',
			cache: false,
			async: false,
            dataType:"jsonp",
			jsonp : "callback",
            data: param,
			contentType: "application/json; charset=UTF-8",
			success: function(data) {
				// 성공적인 응답 처리
				dbk_slider_show(data);
			},
			error: function (request, status, error) {
				console.log('API Error!');
			    console.error('Error:', status, error);
			}
		});


	});
	function dbk_slider_show(data){
		dbkItem = data;
		var itemNo = 0;
		$(document).on('click', '.list-item a.imgLink', function(e) {

			var addHtml = '';
			itemNo = $(this).data('no');

			$(data).each(function(index, item){

				if(item.article_no !== itemNo) {
					addHtml += '<li class="swiper-slide list-item"><a href="#none" class="imgLink" data-no="'+ item.article_no +'"><span class="thumb"><img src="'+ item.url +'" alt="" /></span><div class="desc"><div><strong class="subj">' + item.title +'</strong><p>'+ item.created_date +'</p></div></div></a></li>';
				}
				$(".other-item .swiper-wrapper").html(addHtml);
			});
			if ( !$(".other-item .swiper-wrapper .swiper-slide").length ) {
				$('.other-item').hide();
			}
		});
		
		if( $('#insightLayer .detail-bottom').length ) { //pc
			swiper = new Swiper($('.detail-bottom').find('.swiper-container'), {
				spaceBetween: 13,
				slidesPerView: 4,
				grabCursor: true,
				navigation: {
					nextEl: $('.detail-bottom').find(".swiper-button-next"),
					prevEl: $('.detail-bottom').find(".swiper-button-prev"),
				},
				observer: true,
				observeParents: true,
				initialSlide: 0,
				touchRatio: 0,
				slidesPerGroup : 4,
				speed: 800,
			});
		} else if ( $('#insightLayer .detail-bottom_m').length ) { // 모바일
			swiper = new Swiper($('.detail-bottom_m').find('.swiper-container'), {
				spaceBetween: 13,
				slidesPerView: 2,
				grabCursor: true,
				navigation: {
					nextEl: $('.detail-bottom_m').find(".swiper-button-next"),
					prevEl: $('.detail-bottom_m').find(".swiper-button-prev"),
				},
				observer: true,
				observeParents: true,
				initialSlide: 0,
				slidesPerGroup : 2,
				speed: 800,
			});
		}
	}