<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Services\RelatedProductService;

class RelatedProductController extends BaseController
{
	protected $relatedProductService = null;

    public function __construct()
    {
		if($this->relatedProductService == null) {
			$this->relatedProductService = RelatedProductService::factory();
		}
	}

    public function index()
    {
		$param = [];
		$keywords = $this->request->getGet("filter[keywords]") ?? '';
		$param['filter.keywords'] = $keywords;
		// 전체 판매 상태 조회: ALL_CONDITIONS, 판매대기와 판매중 상품 조회: READY_ONSALE, 판매중 상품만 조회: ONSALE - default, 예약판매중인 상품과 판매중인 상품만 조회
		$param['filter.saleStatus'] = $this->request->getGet("filter[saleStatus]") ?? 'ONSALE';
		// 품절상품 포함 여부
		$param['filter.soldout'] = $this->request->getGet("filter[soldout]") ?? 'false';
		$param['filter.totalReviewCount'] = 'false'; // 총 상품평 수 포함 여부
		$param['filter.familyMalls'] = 'false'; // 서비스에 계약된 모든 쇼핑몰 조회 여부
		// POPULAR:판매인기순 (DEFAULT), SALE_YMD:판매일자, SALE_END_YMD:판매종료일자, DISCOUNTED_PRICE:가격순, REVIEW:상품평, SALE_CNT:총판매량순, RECENT_PRODUCT:최근상품순, MD_RECOMMEND:MD추천순, LIKE_CNT: 좋아요, EXPIRATION_DATE: 유효일자
		$param['order.by'] = $this->request->getGet("order.by") ?? 'POPULAR';
		$param['order.direction'] = $this->request->getGet("order.direction") ?? 'DESC';
		$param['order.soldoutPlaceEnd'] = $this->request->getGet("order.soldoutPlaceEnd") ?? 'false';

		$param['pageNumber'] = $this->request->getGet("page") ?? 1;
		$param['pageSize'] = $this->request->getGet("pageSize") ?? 5;
		$param['onlySaleProduct'] = 'false'; // 세일 상품만 조회 여부
		$param['hasMaxCouponAmt'] = 'false'; // 목록에 최대 할인 쿠폰 가격 포함 여부
		$param['hasTotalCount'] = 'false'; // 목록 카운트 포함 여부
		$param['hasOptionValues'] = 'false'; // 목록에 옵션 value 포함여부
		$param['includeSummaryInfo'] = 'false'; // summary 정보 포함 여부
		$categoryNos = $this->request->getGet("categoryNos") ?? '';
		if($categoryNos != '') {
			$param['categoryNos'] = $categoryNos;
		}
		$req = $param;
		$data = $this->relatedProductService->product_list($param);

		$selectData = [];
		$selectData['saleStatus'] = ['ONSALE' => '판매중인 상품만', 'READY_ONSALE' => '판매대기와 판매중 상품', 'ALL_CONDITIONS' => '전체 판매 상태'];
		$selectData['soldout'] = ['false' => '포함안함', 'true' => '포함함'];
		$selectData['by'] = ['POPULAR'=>'판매인기순', 'SALE_YMD' => '판매일자', 'SALE_END_YMD'=>'판매종료일자', 'DISCOUNTED_PRICE' => '가격순', 
		'REVIEW'=>'상품평', 'SALE_CNT' => '총판매량순', 'RECENT_PRODUCT'=>'최근상품순', 'MD_RECOMMEND' => 'MD추천순', 'LIKE_CNT'=>'좋아요', 'EXPIRATION_DATE' => '유효일자'];
		$selectData['direction'] = ['DESC'=>'등록일↓', 'ASC' => '등록일↑'];
		$selectData['soldoutPlaceEnd'] = ['false' => '사용안함', 'true' => '사용함'];

		$result = [];
		$product_list = $data['items'];
		/*
		foreach($product_list as &$val) {
			$val['productNo'] = explode(':', $val['searchProductId'])[1];
		}
		*/
		$result['product_list'] = $product_list;
		$result['totalCount'] = $data['totalCount'];
		$result['pageCount'] = $data['pageCount'];
		$result['selectData'] = $selectData;
		$result['req'] = $req;

		$pager = service('pager');

        $pager_links = $pager->makeLinks($param['pageNumber'], $param['pageSize'], $result['totalCount'], 'cus_pager');
		$result['pager_links'] = $pager_links;

		$param['filter.soldout'] = $this->request->getGet("filter[soldout]") ?? 'false';
		// 이미 선택된 상품 관련 작업 sjh
		$productNo = $this->request->getGet('productNo') ?? '';
		$promostionStr = $this->request->getGet('promostionStr') ?? '';
		if(isset($productNo) && $productNo != '') {
			$subParam = [];
			$subParam['productNos'] = $productNo;
			$subParam['hasOptionValues'] = 'true';
			$subData = $this->relatedProductService->product_add_list($subParam);
			$result['subProductList'] = $subData;
		} else if($promostionStr != '') {
			// 이미 프로모션에 담겨져 있다면 
			if(isset($promostionStr) && $promostionStr != '') {
				$subParam = [];
				$subParam['productNos'] = explode('^|^', $promostionStr);
				$subParam['hasOptionValues'] = 'true';
				$subData = $this->relatedProductService->product_add_list($subParam);
				$result['subProductList'] = $subData;
			}
		}

		return admin_render('admin/related/index', $result);
    }

	// 검색기능이 없는 함수
    public function index_search_remove()
    {
		$param = [];
		$param['pageNumber'] = $this->request->getGet("page") ?? 1;
		$param['pageSize'] = $this->request->getGet("pageSize") ?? 5;
		// POPULAR:판매인기순 (DEFAULT), SALE_YMD:판매일자, SALE_END_YMD:판매종료일자, DISCOUNTED_PRICE:가격순, REVIEW:상품평, SALE_CNT:총판매량순, RECENT_PRODUCT:최근상품순, MD_RECOMMEND:MD추천순, LIKE_CNT: 좋아요, EXPIRATION_DATE: 유효일자
		$param['order']['by'] = $this->request->getGet("by") ?? 'POPULAR';
		$param['order']['direction'] = $this->request->getGet("direction") ?? 'DESC';

		$data = $this->relatedProductService->product_list($param);

		$result = [];
		$product_list = $data['items'];

		$result['product_list'] = $product_list;
		$result['totalCount'] = $data['totalCount'];
		$result['pageCount'] = $data['pageCount'];
		$result['selectData'] = $selectData;
		$result['req'] = $param;

		$pager = service('pager');

        $pager_links = $pager->makeLinks($param['pageNumber'], $param['pageSize'], $result['totalCount'], 'cus_pager', 0);
		$result['pager_links'] = $pager_links;

		return admin_render('admin/related/index', $result);
    }
}
