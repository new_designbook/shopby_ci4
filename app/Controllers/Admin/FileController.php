<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

class FileController extends BaseController
{
    public function index()
    {
        $files = $this->request->getFiles();
		if(is_array($files) === true) {
			foreach($files as $key => $file) {
				if($file->getError() === 0) { // 파일 첨부가 되었다면
					// APPPATH -> 
					$uplodePath = FCPATH.'uploads';

					$newName = $file->getRandomName();
					$file->move($uplodePath, $newName);
					$data['uploaded'] = true;
					$data['80'] = site_url('/uploads/'.$newName);
					$data['160'] = site_url('/uploads/'.$newName);
					$data['240'] = site_url('/uploads/'.$newName);
					$data['320'] = site_url('/uploads/'.$newName);
					$data['400'] = site_url('/uploads/'.$newName);
					$data['480'] = site_url('/uploads/'.$newName);
					$data['560'] = site_url('/uploads/'.$newName);
					$data['640'] = site_url('/uploads/'.$newName);
					$data['defulte'] = site_url('/uploads/'.$newName);
					$data['url'] = site_url('/uploads/'.$newName);
					echo json_encode($data);
					exit();
				}
			}
		} else {
			if($files->getError() === 0) { // 파일 첨부가 되었다면
				// FCPATH -> public 경로
				// APPPATH -> 
				$uplodePath = FCPATH.'uploads';
				// 기존 파일을 삭제 해야함
				if(isset($postData[$key]) &&  $postData[$key] != '') {
					delete_files($uplodePath.$postData[$key], false);
				}

				$newName = $file->getRandomName();
				$files->move($uplodePath, $newName);
					
				$data['defulte'] = site_url('/uploads/'.$newName);
				echo json_encode($data, true);
				exit;
			}

		}
    }
    public function show($url)
    {
		echo $url;
		exit();
	}
}
