<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\Propose\ProposeModel;
use App\Services\ProposeService;
use App\Entities\ProposeEntities;
use App\Services\FileService;

class Propose extends BaseController
{
    public function index()
    {
        $kind = $this->request->getVar('kind') ?? 'n';
		$page = $this->request->getGet("page_propose") ?? 1;
		$perPage = $this->request->getGet("perPage") ?? 10;
		$orderby = $this->request->getGet("orderby") ?? 'desc';

		list($pager, $propose_list) = ProposeService::factory()->propose_list($page, $kind, $perPage, $orderby);        
		
		$queryParam = [];
		$queryParam['kind'] = $kind;
		$queryParam['page'] = $page;
		$queryParam['perPage'] = $perPage;
		$queryParam['orderby'] = $orderby;


		return admin_render('admin/propose/index', [
			'propose_list' => $propose_list,
			'page_propose' => $page,
			'pager' => $pager,
			'kind' => $kind,
			'orderby' => $orderby,
			'perPage' => $perPage,
			'queryParam' => http_build_query($queryParam),
            'idx' => idx($page, $pager->getPerPage('propose'), $pager->getTotal('propose')),
		]);
    }

	// 조회
	public function show($sno)
	{
        $queryParam = $this->request->getGetPost();
		$queryParam['page_propose'] = $queryParam['page'];

		$proposeService = ProposeService::factory();
		$propose = $proposeService->find($sno);
		if (!$propose) {
			return $this->response->redirect("/admin/propose");
		}
		if(isset($propose->file) && $propose->file != '' ) {
            $fileService = FileService::factory();
			$fileInfo = $fileService->fileInfo($propose->file);
			$propose->fileInfo = $fileInfo;
		}
		return admin_render('admin/propose/show', [
			'propose' => $propose->toArray(),
			'queryParam' => http_build_query($queryParam),
		]);
	}

    // 삭제
	public function delete()
	{
		if ($this->request->getMethod() !== "post"){
			return $this->response->redirect("/admin/propose");
		}
	
		$sno = $this->request->getPost('sno');
		
		if(ProposeService::factory()->delete($sno)) {
			helper('alert');
			alert_back("삭제가 완료되었습니다.");
			return $this->response->redirect("/admin/propose");
		} else {
			helper('alert');
			alert_back("문제가 발생하였습니다.<br/>새로고침 이후 진행 바랍니다.");
			return $this->response->redirect("/admin/propose/show/".$sno);

		}
	}

    // 수정
    public function edit($sno)
	{
		$proposeService = ProposeService::factory();
		$propose = $proposeService->find($sno);
		if (!$propose) {
			helper('alert');
			alert_back("수정이 실패하였습니다.");
			$this->response->redirect("/admin/propose/index");
		}

		list($isSuccess, $errors) = $proposeService->update($propose, $this->request->getPost());
		if ($isSuccess){
			helper('alert');
			alert_back("수정이 완료되었습니다.");
			$this->response->redirect("/admin/propose/show/$sno");
		} else {
			helper('alert');
			alert_back("수정이 실패하였습니다.");
			$this->response->redirect("/admin/propose/index");
			
		}
	}

	// 첨부파일 다운로드
	public function getFileDownload($sno)
	{
		/*
		$this->load->helper('download');

		echo print_r($propose,true);exit;
		*/
		$fileService = FileService::factory();
		$file = $fileService->find($sno);
		
		return $this->response->download($file['pathName']."\\" .$file['name'] );

	}
}
