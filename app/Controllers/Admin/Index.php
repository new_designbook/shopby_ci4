<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

class Index extends BaseController
{
    public function index()
    {
		// Basic b1pZTENjWnlNVmtodEl3bEp6VVJwQTpsQ2RoUzNiaU10QmtGS2R2Snl6RzNB
        // return view('admin/index');
        
        $session = \Config\Services::session();
		$session->set('user_role', 'admin');

        return admin_render('admin/index');
    }
}