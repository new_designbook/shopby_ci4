<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\CouponModel;
use App\Services\CouponService;
use App\Entities\CouponEntities;

class CouponController extends BaseController
{
    public function index()
    {
    }

	// 조회
	public function show($sno)
	{
		$couponService = CouponService::factory();
		$coupon = $couponService->find($sno);
		if (!$coupon) {
			return $this->response->redirect("/admin/coupon");
		}

		return admin_render('admin/coupon/index', [
			'coupon' => $coupon->toArray()
		]);
	}

    // 수정
    public function edit()
	{
		$sno = 1;
		//앞에 소문자는 m :model, s:Service, e:Entities
		$mCoupon = new \App\Models\CouponModel();
		$postData = $this->request->getPost();
		if($postData) {
			$eCoupon = new \App\Entities\CouponEntities($postData);
			try
			{
				$mCoupon->update($eCoupon->sno, $eCoupon);
				helper('alert');
				alert_move("저장이 완료되었습니다.",'/admin/coupon');
			}
			catch (\ReflectionException $e)
			{
				helper('alert');
				alert_move("수정이 실패하였습니다.",'/admin/coupon');
			}
		} else {

		}
		/*
		$couponService = CouponService::factory();
		$coupon = $couponService->find($sno);

		if (!$coupon) {
			helper('alert');
			alert_back("수정이 실패하였습니다.");
			$this->response->redirect("/admin/coupon/index");
		}
		list($isSuccess, $errors) = $couponService->update($coupon, $this->request->getPost());
		if ($isSuccess){
			helper('alert');
			alert_back("수정이 완료되었습니다.");
			$this->response->redirect("/admin/coupon/show/$sno");
		} else {
			helper('alert');
			alert_back("수정이 실패하였습니다.");
			$this->response->redirect("/admin/propose/show", [$errors]);
		}
		*/
	}

}
