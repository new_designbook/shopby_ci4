<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\IconModel;
use App\Services\IconService;
use App\Entities\IconEntities;

class IconController extends BaseController
{
    public function index()
    {
    }

	// 조회
	public function show($sno)
	{
		$iconService = IconService::factory();
		$icon = $iconService->find($sno);
		if (!$icon) {
			return $this->response->redirect("/admin/icon");
		}
		$tmpArr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
		return admin_render('admin/icon/index', [
			'icon' => $icon->toArray(),
			'keyArray' => $tmpArr
		]);
	}

    // 수정
    public function edit()
	{
		$sno = 1;
		//앞에 소문자는 m :model, s:Service, e:Entities
		$mIcon = new \App\Models\IconModel();
		$postData = $this->request->getPost();
		if($postData) {
			$eIcon = new \App\Entities\IconEntities($postData);
			try
			{
				$mIcon->update($eIcon->sno, $eIcon);
				helper('alert');
				alert_move("저장이 완료되었습니다.",'/admin/icon');
			}
			catch (\ReflectionException $e)
			{
				helper('alert');
				alert_move("수정이 실패하였습니다.",'/admin/icon');
			}
		} else {

		}
		/*
		$iconService = IconService::factory();
		$icon = $iconService->find($sno);

		if (!$icon) {
			helper('alert');
			alert_move("수정이 실패하였습니다.", "/admin/icon/index");
		}

		list($isSuccess, $errors) = $iconService->update($icon, $this->request->getPost());

		if ($isSuccess){
			helper('alert');
			alert_move("수정이 완료되었습니다.", "/admin/icon");
		} else {
			helper('alert');
			alert_move("수정이 실패하였습니다.", "/admin/icon");	
		}
		*/
	}
}
