<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\UserModel;

class Temp extends BaseController
{
    protected $bm;
    protected $perPage = 10;

    public function __construct()
    {
        $this->bm = new UserModel();    
    }

    public function index() :string
    {
        $pageNum = $this->request->getVar('page_num');
        if(!$pageNum) $pageNum = 1;

        $bm = $this->bm->getPagination($this->perPage);
        $data = [
            'list_data' => $bm['data'],
            'pager' => $bm['pager'],
            'idx' => idx($pageNum,$this->perPage,$bm['totalCnt']),
        ];

        return admin_render('admin\temp', $data);
    }
}