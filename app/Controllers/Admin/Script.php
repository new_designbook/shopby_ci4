<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Services\ScriptService;

class Script extends BaseController
{
    public function index()
    {
		$data = ScriptService::factory()->getScript();

		if(!empty($data['scripttags'])){
			$script = $data['scripttags'][0]['script_no'];
		} else{
			$script = 0;
		}
		
		return admin_render('admin/script/index', [
			'script' => $script,
		]);
    }
	
	public function create()
	{
		$data = ScriptService::factory()->create();
		echo print_r($data,true);exit;
		if(!empty($data['scripttags'])){
			$script = $data['scripttags'][0]['script_no'];
		} else{
			$script = 0;
		}
		return admin_render('admin/script/index', [
			'script' => $script,
		]);

	}
	
	public function scriptDelete($script_no)
	{
		$data = ScriptService::factory()->scriptDelete($script_no);

		if(!empty($data['scripttags'])){
			$script = $data['scripttags'][0]['script_no'];
		} else{
			$script = 0;
		}
		return admin_render('admin/script/index', [
			'script' => $script,
		]);

	}

}
