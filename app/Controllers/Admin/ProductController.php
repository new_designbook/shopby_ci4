<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Services\ProductService;
use App\Models\ProductModel;
use App\Entities\ProductEntities;

class ProductController extends BaseController
{
	/**
	 * Undocumented function
	 * index 리스트(나중에 list로 바꿀지는 확인 필요)
	 * @return void
	 */
    public function index()
    {
		$pager = service('pager');

		$page = $this->request->getGet("page") ?? 1;
		$perPage = $this->request->getGet("perPage") ?? 10;
		$orderby = $this->request->getGet("orderby") ?? 'desc';

        $param = [
            'page' => $page,
            'size' => $perPage,
            'order' => [
                'direction' => 'RECENT_PRODUCT',
                'by' => $orderby
            ]
        ];
        
		//1차 컨트롤러 안에 curl 넣고
		//2차 범용으로 사용할 수 있게 BaseController를 구성해서 할지 생각해봅시다
        $client = service('curlrequest');

		$posts_data = $client->get("https://server-api.e-ncp.com/products/search?".http_build_query($param, '', ), [
			"headers" => [
				"Accept" => "application/json",
                'Authorization' => 'Bearer '.$_ENV['app.Token'],
                'systemkey' => $_ENV['app.SystemKey'],
                'mallKey' => $_ENV['app.Outkey'],
                'version' => ' 1.0'
			]
		]);

		//echo $posts_data->getReason(); 상태값
		//echo $posts_data->getStatusCode(); 상태코드
		//print_r($posts_data->getBody());
        $productList = json_decode($posts_data->getBody(), true);

		//샵바이 API에서 상품 리스트 호출 
        $total   = $productList['totalCount'];
		//페이징 템플릿 cur_pager 호출
        $pager_links = $pager->makeLinks($page, $perPage, $total,'cus_pager');

		return admin_render('admin/product/list', [
			'list' => $productList,
            'pager_links' => $pager_links,
			'orderby' => $orderby,
			'perPage' => $perPage,
			'page' => $page,
		]);

		/*
		//샵바이 API에서 상품 리스트 호출 
		$list = ProductService::factory()->product_list($page, $perPage, $orderby);
        $total   = $list['totalCount'];
		//페이징 템플릿 cur_pager 호출
        $pager_links = $pager->makeLinks($page, $perPage, $total,'cus_pager');

		return admin_render('admin/product/list', [
			'list' => $list,
            'pager_links' => $pager_links,
			'orderby' => $orderby,
			'perPage' => $perPage,
			'page' => $page,
		]);
		*/
    }

	/**
	 * show는 상세
	 *
	 * @param [type] $product_no
	 * @return void
	 */
	public function show($product_no)
	{
		$page = $this->request->getGet("page") ?? 1;
		$mProduct = new ProductModel();
		$eProduct = new ProductEntities();

		$eProduct = $mProduct->where('product_no',$product_no)->find()[0]??null;

		return admin_render('admin/product/view', [
			'data' => $eProduct,
			'product_no' => $product_no,
			'page' => $page,
		]);
	}

	/**
	 * Undocumented function
	 * 수정일 경우, 저장 및 이런건 추 후 고도화
	 * update 같은 경우 model 의 primaryKey를 조건으로 진행
	 * Where 절이 들어갈 경우 where -> set 으로 추가진해해줘야함
	 * @return void
	 */
    public function edit()
	{
		//앞에 소문자는 m :model, s:Service, e:Entities
		$mProduct = new ProductModel();
		
		$postData = $this->request->getPost();
		if($postData['product_no']) {
			$eProduct = new ProductEntities($postData);
			if($mProduct->where('product_no', $postData['product_no'])->countAllResults() == 0) {
				try
				{
					$mProduct->insert($eProduct);
					helper('alert');
					alert_move("저장이 완료되었습니다.",'/admin/product/show/'.$postData['product_no']);
				}
				catch (\ReflectionException $e)
				{
					helper('alert'+ $e);
					alert_move("저장이 실패하였습니다.",'/admin/product');
				}	
			}else {
				try
				{
					$mProduct->update($postData['product_no'], $eProduct);
					helper('alert');
					alert_move("수정이 완료되었습니다.",'/admin/product/show/'.$postData['product_no'].'?page='.$postData['page']);
				}
				catch (\ReflectionException $e)
				{
					helper('alert'+ $e);
					alert_move("수정이 실패하였습니다.",'/admin/product');
				}			
			}
		}else {

		}
	}

}
