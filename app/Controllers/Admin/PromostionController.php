<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\PromostionModel;
use App\Entities\PromostionEntities;
use App\Services\RelatedProductService;

class PromostionController extends BaseController
{
    public function index()
    {
		$page = $this->request->getGet("page_promostion") ?? 1;
		$perPage = $this->request->getGet("perPage") ?? 10;
		$orderby = $this->request->getGet("orderby") ?? 'desc';

        $mPromostion = new PromostionModel();
        $mPromostion->orderBy("created_at", $orderby);

        $promostion_list = $mPromostion->paginate($perPage, 'promostion', $page);

        $pager = $mPromostion->pager;

		$queryParam = [];
		$queryParam['page'] = $page;
		$queryParam['perPage'] = $perPage;
		$queryParam['orderby'] = $orderby;

		$nowDate = strtotime(date("Y-m-d H:i:s"));
		foreach($promostion_list as $key => &$val) {			
            $_eventStart = strtotime($val->eventStart);
            $_eventEnd = strtotime($val->eventEnd);
			$status = '진행전';
            if ($nowDate < $_eventStart) {
                $status = '진행전';
            } else if ($nowDate > $_eventStart && $nowDate < $_eventEnd) {
                $status = '진행중';
            } else if ($nowDate > $_eventEnd) {
                $status = '종료';
            }
			$val->status = $status;
		}

		return admin_render('admin/promostion/index', [
			'promostion_list' => $promostion_list,
			'page_promostion' => $page,
			'pager' => $pager,
			'orderby' => $orderby,
			'perPage' => $perPage,
			'queryParam' => http_build_query($queryParam),
            'idx' => idx($page, $pager->getPerPage('promostion'), $pager->getTotal('promostion')),
		]);
    }

	// 조회
	public function show($sno)
	{
        $queryParam = $this->request->getGetPost();
		$queryParam['page_propose'] = $queryParam['page'] ?? 1;

		$randerData = [];
		$title = "등록";
		if($sno != '0') {
			$title = "수정";
			$mPromostion = new \App\Models\PromostionModel();
			$promostion = $mPromostion->find($sno);
			$data = $promostion->toArray();
			
			$eventStartObj = date_create($data['eventStart']);
			$eventEndObj = date_create($data['eventEnd']);
			$data['eventStartInfo']['date'] = date_format($eventStartObj, "Y-m-d");
			$data['eventStartInfo']['hour'] = date_format($eventStartObj, "H");
			$data['eventStartInfo']['minute'] = date_format($eventStartObj, "i");
			$data['eventEndInfo']['date'] = date_format($eventEndObj, "Y-m-d");
			$data['eventEndInfo']['hour'] = date_format($eventEndObj, "H");
			$data['eventEndInfo']['minute'] = date_format($eventEndObj, "i");
			
			$product = $data['product'];
			if(isset($product) && $product != '') {
				$relatedProductService = RelatedProductService::factory();
				$subParam = [];
				$subParam['productNos'] = explode('^|^', $product);
				$subParam['hasOptionValues'] = 'true';
				$subData = $relatedProductService->product_add_list($subParam);
				$randerData['subProductList'] = $subData;
			}
			$randerData['promostion'] = $data;
		}
		$randerData['title'] = $title;
		$randerData['sno'] = $sno;
		$randerData['queryParam'] = http_build_query($queryParam);
		$hour = [];
		for($i = 0; $i < 24; $i++) {
			$hour[] = sprintf('%02d',$i);
		}
		
		$minute = [];
		for($i = 0; $i < 60; $i++) {
			$minute[] = sprintf('%02d',$i);
		}
		$randerData['hour'] = $hour;
		$randerData['minute'] = $minute;

		$viewCnt = [];
		for($i = 0; $i < 11; $i++) {
			$viewCnt[] = $i;
		}
		$randerData['viewCnt'] = $viewCnt;
		return admin_render('admin/promostion/show', $randerData);
	}

    // 수정
    public function edit($sno)
	{
		helper('filesystem');
		//앞에 소문자는 m :model, s:Service, e:Entities
		$mPromostion = new \App\Models\PromostionModel();
		$postData = $this->request->getPost();
        $files = $this->request->getFiles();
		if(isset($postData['fileDel'])) {
			$uplodePath = FCPATH."uploads/";
			foreach($postData['fileDel'] as $key => $val) {
				if(file_exists($uplodePath.$val)) {
					unlink($uplodePath.$val);
					$postData[$key] = "";
				}
			}
		}
		// 첨부파일 관련 sjh
		foreach($files as $key => $file) {
			if($file->getError() === 0) { // 파일 첨부가 되었다면
				// FCPATH -> public 경로
				// APPPATH -> 
				$uplodePath = FCPATH."uploads/";
				// 기존 파일을 삭제 해야함
				if(isset($postData[$key]) &&  $postData[$key] != '' && file_exists($uplodePath.$postData[$key])) {
					unlink($uplodePath.$postData[$key]);
				}

				$newName = $file->getRandomName();
				$file->move($uplodePath, $newName);
				$postData[$key] = $newName;
			}
		}
		// 시간 관련 sjh
		$postData['eventStart'] = $postData['eventStart'].' '.$postData['eventStartHour'].':'.$postData['eventStartMinute'].':00';
		$postData['eventEnd'] = $postData['eventEnd'].' '.$postData['eventEndHour'].':'.$postData['eventEndMinute'].':59';
		unset($postData['eventStartHour'], $postData['eventStartMinute'], $postData['eventEndHour'], $postData['eventEndMinute']);
		$postData['product'] = $postData['product'];

		if($postData) {
			$ePromostion = new \App\Entities\PromostionEntities($postData);
			try
			{
				if($postData['sno'] == '0') {
					$mPromostion->insert($ePromostion);
					helper('alert');
					alert_move("저장이 완료되었습니다.",'/admin/promostion');
				} else {
					$mPromostion->update($ePromostion->sno, $ePromostion);
					helper('alert');
					alert_move("저장이 완료되었습니다.",'/admin/promostion/show/'.$postData['sno'] );
				}
			}
			catch (\ReflectionException $e)
			{
				helper('alert');
				alert_move("수정이 실패하였습니다.",'/admin/promostion');
			}
		} else {

		}
	}

    // 삭제
	public function delete()
	{
		helper('filesystem');
		helper('alert');
		if ($this->request->getMethod() !== "post"){
			alert_move("잘못된 접근입니다.",'/admin/promostion');
			return false;
		}
		$mPromostion = new \App\Models\PromostionModel();
		$sno = $this->request->getPost('sno');

		$promostion = $mPromostion->find($sno);
		
		// APPPATH -> 
		$uplodePath = FCPATH."uploads/";
		// 기존 파일을 삭제 해야함
		if(isset($promostion->eventThumbnail) &&  $promostion->eventThumbnail != '') {
			if(file_exists($uplodePath.$promostion->eventThumbnail)) unlink($uplodePath.$promostion->eventThumbnail);
		}
		if(isset($promostion->eventThumbnailMobile) &&  $promostion->eventThumbnailMobile != '') {
			if(file_exists($uplodePath.$promostion->eventThumbnailMobile)) unlink($uplodePath.$promostion->eventThumbnailMobile);
		}
		if(isset($promostion->eventImage) &&  $promostion->eventImage != '') {
			if(file_exists($uplodePath.$promostion->eventImage)) unlink($uplodePath.$promostion->eventImage);
		}
		if(isset($promostion->eventImageMobile) &&  $promostion->eventImageMobile != '') {
			if(file_exists($uplodePath.$promostion->eventImageMobile)) unlink($uplodePath.$promostion->eventImageMobile);
		}
		
		if($mPromostion->delete($sno)) {
			alert_move("삭제가 완료되었습니다.",'/admin/promostion');
		} else {
			alert_move("문제가 발생하였습니다.<br/>새로고침 이후 진행 바랍니다.","/admin/propose/show/".$sno);
		}
		exit();
	}

}
