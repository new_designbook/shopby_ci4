<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Services\MovieService;

class MovieContorller extends BaseController
{
    public function index()
    {
    }

	// 조회
	public function show($sno)
	{
		$movieService = MovieService::factory();
		$movie = $movieService->find($sno);
		
		return admin_render('admin/movie/index', [
			'data' => $movie
		]);
	}

	/**
	 * Undocumented function
	 * 수정일 경우, 저장 및 이런건 추 후 고도화
	 * @return void
	 */
    public function edit()
	{
		//앞에 소문자는 m :model, s:Service, e:Entities
		$mMovie = new \App\Models\MovieModel();
		$postData = $this->request->getPost();
		if($postData) {
			$eMovie = new \App\Entities\MovieEntities($postData);
			try
			{
				$mMovie->update($eMovie->sno,$eMovie);
				helper('alert');
				alert_move("저장이 완료되었습니다.",'/admin/movie');
			}
			catch (\ReflectionException $e)
			{
				helper('alert');
				alert_move("수정이 실패하였습니다.",'/admin/movie');
			}
		}else {

		}
		
		
		

		/*
		$movie = $movieService->find($sno);
		if (!$movie) {
			helper('alert');
            alert_move("수정이 실패하였습니다.",'/admin/movie');
		}
		list($isSuccess, $errors) = $movieService->update($movie, $this->request->getPost());
		if ($isSuccess){
			helper('alert');
            alert_move("저장이 완료되었습니다.",'/admin/movie');
		} else {
			helper('alert');
            alert_move("수정이 실패하였습니다.",'/admin/movie');
		}
		*/
	}
}
