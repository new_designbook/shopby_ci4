<?php

namespace App\Controllers\Front;

use App\Controllers\BaseController;
use App\Services\CouponService;

class CouponApiController extends BaseController
{
    public function __construct()
    { 
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
        header("Access-Control-Allow-Headers: *");
        header('Content-Type: application/javascript');
    }

    public function index()
    {
        // 추후 productNo 를 추가 할 수 있음
        $callback = $this->request->getVar('callback');
        // 파라미터가 필요없으면 주석 할 예정 sjh
        $productNo = $this->request->getVar('productNo');
        $sno = '1';
                
        $couponService = CouponService::factory();
        $coupon = $couponService->frontFind($sno);
        $result = [];
        $result['code'] = '404';
        if(!$coupon) {
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
            header("Access-Control-Allow-Headers: *");
            header('Content-Type: application/javascript');

            $result['useFl'] = 'n';
            $result['couponNo'] = '';
            $result['productNo'] = '';
            $result['productNo2'] = $productNo;

        } else {
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
            header("Access-Control-Allow-Headers: *");
            header('Content-Type: application/javascript');
            if($productNo && $coupon->productNo == $productNo) {
                $result['code'] = '204';
                $result['useFl'] = 'n';
                $result['couponNo'] = $coupon->couponNo;
                $result['productNo'] = $coupon->productNo;
                $result['productNo2'] = $productNo;
            } else {
                $result['code'] = '200';
                $result['useFl'] = 'y';
                $result['couponNo'] = $coupon->couponNo;
                $result['productNo'] = $coupon->productNo;
                $result['productNo2'] = $productNo;
            }
        }
        echo $callback.'('.json_encode($result, JSON_UNESCAPED_UNICODE).')';
		exit;
    }
}

/**
 *  디자인교과서 html 에서 사용될 스크립트 (jQuery 가 있다는 가정) sjh
 
    $('#dbk_ajax').on('click', function(){
        var data = { 'productNo' : '0011' };
        $.ajax({
            url: 'http://localhost:8080/front/coupon_info',
            type: 'GET',
            cache: false,
            async: false,
            data: data,
            dataType:"jsonp",
            jsonp : "callback",
            contentType: "application/json; charset=UTF-8",
            success: function(data) {
                // 성공적인 응답 처리
                console.log(data);
            },
            error: function (request, status, error) {
                console.log('API Error!');
                console.error('Error:', status, error);
            }
        });

    });
 * 
 */
