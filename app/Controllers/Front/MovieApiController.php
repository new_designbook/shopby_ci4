<?php

namespace App\Controllers\Front;

use App\Controllers\BaseController;
use App\Services\MovieService;

class MovieApiController extends BaseController
{
    public function __construct()
    { 
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
        header("Access-Control-Allow-Headers: *");
        header('Content-Type: application/javascript');
    }

    public function index()
    {
        //파라미터가 넘어올 시 필요
        $callback = $this->request->getVar('callback');
        $sno = '1';

        $movieService = MovieService::factory();
        $movie = $movieService->find($sno);

        $result = [];
        $result['code'] = '404';
        if(!$movie) {

        } else {
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
            header("Access-Control-Allow-Headers: *");
            header('Content-Type: application/javascript');
            $result['code'] = '200';
            $result['pc_movie_url_1'] = urlencode($movie->pc_movie_url_1);
            $result['pc_movie_url_2'] = urlencode($movie->pc_movie_url_2);
            $result['mo_movie_url_1'] = urlencode($movie->mo_movie_url_1);
            $result['mo_movie_url_2'] = urlencode($movie->mo_movie_url_2);
        }
       
        echo $callback.'('.json_encode($result, JSON_UNESCAPED_UNICODE).')';
		exit;
    }
}