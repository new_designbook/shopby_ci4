<?php

namespace App\Controllers\Front;

use App\Controllers\BaseController;
use App\Models\ProductModel;
use App\Entities\ProductEntities;

class ProductApiController extends BaseController
{
    public function __construct()
    { 
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
        header("Access-Control-Allow-Headers: *");
        header('Content-Type: application/javascript');
    }

    //리스트용
    public function list()
    {
        //상품군이 적기 때문에 전체를 호출하는 방식으로 진행함
        //상품군이 많을 경우 방법에 대한 정의필요

        //콜백에 대한 정의 필요
        $callback = $this->request->getVar('callback');
        $mProduct = new ProductModel();
        $eProduct = new ProductEntities();
        
        //전체 검색
        $eProduct = $mProduct->findAll()??null;
        
        $result = [];

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
        header("Access-Control-Allow-Headers: *");
        header('Content-Type: application/javascript');

        if(!$eProduct) {
            $result['code'] = '404';
        } else {
            foreach($eProduct as $eKey => $eVal) {
                $result[$eKey]['code'] = '200';
                $result[$eKey]['product_no'] = $eVal->product_no;
                $result[$eKey]['list_movie_link'] = urlencode($eVal->list_movie_link);
            }
        }
       
        echo $callback.'('.json_encode($result, JSON_UNESCAPED_UNICODE).')';
		exit;
    }
        
    //상세용 
    public function show($product_no)
    {
        //파라미터가 넘어올 시 필요
        $callback = $this->request->getVar('callback');
        $mProduct = new ProductModel();
        $eProduct = new ProductEntities();
        
        $eProduct = $mProduct->where('product_no',$product_no)->find()[0]??null;
        
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
        header("Access-Control-Allow-Headers: *");
        header('Content-Type: application/javascript');
        
        $result = [];
        $result['code'] = '404';
        if(!$eProduct) {

        } else {
            $result['code'] = '200';
            $result['detail_movie_link_1'] = urlencode($eProduct->detail_movie_link_1);
            $result['detail_movie_link_2'] = urlencode($eProduct->detail_movie_link_2);
        }
       
        echo $callback.'('.json_encode($result, JSON_UNESCAPED_UNICODE).')';
		exit;
    }
}