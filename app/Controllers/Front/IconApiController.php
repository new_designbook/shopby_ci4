<?php

namespace App\Controllers\Front;

use App\Controllers\BaseController;
use App\Services\IconService;

class IconApiController extends BaseController
{
    public function __construct()
    { 
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
        header("Access-Control-Allow-Headers: *");
        header('Content-Type: application/javascript');
    }

    public function index()
    {
        // 추후 productNo 를 추가 할 수 있음
        $callback = $this->request->getVar('callback');
        $sno = '1';

		$iconService = IconService::factory();
		$icon = $iconService->find($sno);

		$tmpArr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

        $result = [];
        $result['code'] = '404';
        if(!$icon) {
            $result['couponNo'] = '';
            $result['productNo'] = '';
            $result['productNo2'] = $productNo;

        } else {
            $iconArray = $icon->toArray();
            $tmpArr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
            $data = [];
            foreach($tmpArr as $key => $val) {
                $strIcon = $iconArray['icon'.$val];
                if(isset($strIcon) && $strIcon != '') {
                    $data[$key] = explode(',', $strIcon);
                } else {
                    $data[$key] = [];
                }
            }
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
            header("Access-Control-Allow-Headers: *");
            header('Content-Type: application/javascript');
            $result['code'] = '200';
            $result['data'] = $data;
        }
        echo $callback.'('.json_encode($result, JSON_UNESCAPED_UNICODE).')';
		exit;
    }
}

/**
 *  디자인교과서 html 에서 사용될 스크립트 (jQuery 가 있다는 가정) sjh
 
    $('#dbk_ajax').on('click', function(){
        // 추후  sno 가 필요한 경우(3/5/9 번들마다 관리한다고 할때 데이터 추가해서 보내야함)
		$.ajax({
			url: 'http://localhost:8080/front/icon_info',
			type: 'GET',
			cache: false,
			async: false,
			dataType:"jsonp",
			jsonp : "callback",
			contentType: "application/json; charset=UTF-8",
			success: function(data) {
				// 성공적인 응답 처리
				console.log(data);
			},
			error: function (request, status, error) {
				console.log('API Error!');
				console.error('Error:', status, error);
			}
		});
    });
 * 
 */
