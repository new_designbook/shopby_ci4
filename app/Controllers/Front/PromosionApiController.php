<?php

namespace App\Controllers\Front;

use App\Controllers\BaseController;
use App\Services\PromostionService;
use App\Models\PromostionModel;
use App\Services\RelatedProductService;

class PromosionApiController extends BaseController
{
    protected $imgHost = "https://kissnewyorkollio.co.kr/uploads/";

    public function __construct()
    { 
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
        header("Access-Control-Allow-Headers: *");
        header('Content-Type: application/javascript');
    }

    public function index()
    {
        // 추후 productNo 를 추가 할 수 있음
        $callback = $this->request->getVar('callback');

		$page = $this->request->getGet("page") ?? 1;
		$perPage = $this->request->getGet("pageSize") ?? 10;
		$orderby = $this->request->getGet("orderby") ?? 'desc';

        $mPromostion = new PromostionModel();
        $mPromostion->orderBy("created_at", $orderby);

        $promostion_list = $mPromostion->paginate($perPage, 'promostion', $page);

        $pager = $mPromostion->pager;

		$queryParam = [];
		$queryParam['page'] = $page;
		$queryParam['perPage'] = $perPage;
		$queryParam['orderby'] = $orderby;

        $result = [];
        $result['code'] = '404';
        if(!$promostion_list) {
            $data = [];
            $data['promostion_list'] = [];
            $data['totalCnt'] = 0;
            $data['perPage'] = $perPage;
        } else {
            $nowDate = strtotime(date("Y-m-d H:i:s"));
            foreach($promostion_list as $key => &$val) {			
                $_eventStart = strtotime($val->eventStart);
                $_eventEnd = strtotime($val->eventEnd);
                $status = '진행전';
                if ($nowDate < $_eventStart) {
                    $status = '진행전';
                    $statusCode = 'wait';
                } else if ($nowDate > $_eventStart && $nowDate < $_eventEnd) {
                    $status = '진행중';
                    $statusCode = 'active';
                } else if ($nowDate > $_eventEnd) {
                    $status = '종료';
                    $statusCode = 'end';
                }
                $val->statusCode = $statusCode;
                $val->status = $status;
                if(isset($val->eventThumbnail) && $val->eventThumbnail != '') {
                    $val->eventThumbnail = $this->imgHost.$val->eventThumbnail;
                }
                if(isset($val->eventThumbnailMobile) && $val->eventThumbnailMobile != '') {
                    $val->eventThumbnailMobile = $this->imgHost.$val->eventThumbnailMobile;
                }
            }
            $data = [];
            $data['promostion_list'] = $promostion_list;
            $data['totalCnt'] = $pager->getTotal('promostion');
            $data['perPage'] = $perPage;

            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
            header("Access-Control-Allow-Headers: *");
            header('Content-Type: application/javascript');
            $result['code'] = '200';
            $result['data'] = $data;
        }
        echo $callback.'('.json_encode($result, JSON_UNESCAPED_UNICODE).')';
		exit;
    }
    
    public function show($sno)
    {
        $callback = $this->request->getVar('callback');

        $mPromostion = new PromostionModel();
        $promostion = $mPromostion->find($sno);

        $result = [];
        $result['code'] = '404';
        if(!$promostion) {
            $result['data'] = [];
            $result['error'] = '잘못된 접근 입니다.';
        } else {
            $nowDate = strtotime(date("Y-m-d H:i:s"));		
            $_eventStart = strtotime($promostion->eventStart);
            $_eventEnd = strtotime($promostion->eventEnd);
			$statusCode = 'end';
            if ($nowDate < $_eventStart) {
                $statusCode = 'wait';
            } else if ($nowDate > $_eventStart && $nowDate < $_eventEnd) {
                $statusCode = 'active';
            } else if ($nowDate > $_eventEnd) {
                $statusCode = 'end';
            }
            if($statusCode == 'end') {
                $result['code'] = '400';
                $result['error'] = '이미 종료된 기획전입니다.';
            } else {
                if(isset($promostion->product) && $promostion->product != '') {
                    $sRelatedProductService = RelatedProductService::factory();
                    $subParam = [];
                    $subParam['productNos'] = explode('^|^', $promostion->product);
                    $subParam['hasOptionValues'] = 'true';
                    $subData = $sRelatedProductService->product_add_list($subParam);
                    $promostion->subProductList = $subData;
                } else {
                    $promostion->subProductList = [];
                }
                if(isset($promostion->eventImage) && $promostion->eventImage != '') {
                    $promostion->eventImage = $this->imgHost.$promostion->eventImage;
                }
                if(isset($promostion->eventImageMobile) && $promostion->eventImageMobile != '') {
                    $promostion->eventImageMobile = $this->imgHost.$promostion->eventImageMobile;
                }
                $result['code'] = '200';
                $result['data']['promostion'] = $promostion;
            }
        }
        echo $callback.'('.json_encode($result, JSON_UNESCAPED_UNICODE).')';
		exit;
    }
}

/**
 *  디자인교과서 html 에서 사용될 스크립트 (jQuery 가 있다는 가정) sjh
 
    $('#dbk_ajax').on('click', function(){
		$.ajax({
			url: 'http://localhost:8080/front/promostion_list',
			type: 'GET',
			cache: false,
			async: false,
			dataType:"jsonp",
			jsonp : "callback",
			contentType: "application/json; charset=UTF-8",
			success: function(data) {
				// 성공적인 응답 처리
				console.log(data);
			},
			error: function (request, status, error) {
				console.log('API Error!');
				console.error('Error:', status, error);
			}
		});
    });
 * 
 */
