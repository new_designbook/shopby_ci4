<?php

namespace App\Controllers\Front;

use App\Controllers\BaseController;
use App\Models\Cafe24Token;

class Articles extends BaseController
{
    protected $session;
    protected $tokenDb;

    public function __construct()
    {
        $this->session = \Config\Services::session();
        $this->tokenDb = new Cafe24Token();
    }
    
    public function index()
    {
        $callback = $this->request->getVar('callback');
        $board_no = $this->request->getVar('board_no') ?? 8;
		// $board_no = 8;
		$board_category_no = $this->request->getVar('category_no') ?? null; // 세부 카테고리 1~6 
		$param = [];
		$param['limit'] = 100;
		if(isset($board_category_no)) {
			$param['board_category_no'] = $board_category_no;
		}
		$param['attached_file'] = 'T';
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => sprintf('https://%s.cafe24api.com/api/v2/admin/boards/%s/articles', 'egongegong2020', $board_no).'?'.http_build_query($param, '', ),
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->getToken('egongegong2020'),
			'Content-Type: application/json'
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		if ($err) {
			echo 'cURL Error #:' . $err;
		} else {

			$articles = json_decode($response, true)['articles'];

			$result = [];
			if(isset($articles)) {
				$index = 0;
				foreach($articles as $key => $article) {
					if(isset($article['attach_file_urls'][0])) {
						$result[$index]['article_no'] = $article['article_no'];
						$result[$index]['title'] = $article['title'];
						$result[$index]['url'] = $article['attach_file_urls'][0]['url'];
						$result[$index]['name'] = $article['attach_file_urls'][0]['name'];
						$result[$index]['content'] = $article['content'];
						$result[$index]['writer'] = $article['writer'];
						$result[$index]['hit'] = $article['hit'];
						// $result[$index]['created_date'] = $article['created_date'];
						$result[$index]['created_date'] = date("Y-d-m", strtotime($article['created_date']));
						$index++;
					}
				}
			}
			header('Access-Control-Allow-Origin: *');
			header("Access-Control-Allow-Methods: POST,GET, OPTIONS");
			header("Access-Control-Allow-Headers: *");
			header('Content-Type: application/javascript');

			/*
			$method = $_SERVER['REQUEST_METHOD'];
			if($method == "OPTIONS") {
				die();
			}
			return $this->response->setJSON($result);
        	$json_object = json_decode($result, true);
			$json_output = json_encode($json_object, JSON_UNESCAPED_UNICODE);
			*/
			// return $this->response->setJSON($result);
			echo $callback.'('.json_encode($result, JSON_UNESCAPED_UNICODE).')';
		}
		exit;
    }

	
    protected function getToken($mallId)
    {
        //토큰 만료 체크를 위한 시간설정 (현재 시간보다 2시간뒤 시간체크 - 작업을 체크하기 위함)
        $timenow = date("Y-m-d H:i:s");
        $twoTimenow = date("Y-m-d His", strtotime($timenow."+30 minutes"));
        
        //몰아이디
        // $this->session->set('mall_id', $mallId);

        //토큰 확인 후 없으면 신규발급n, 있으면 사용u, 만료면e 재발급 분기로 구분
		$token = $this->tokenDb->GetClientTokenInfo($mallId);//배열로 받아오기
        //토큰이 사용 가능할 시

        if($token)
        {
            if($token->access_token && strtotime($token->expires_at) >= strtotime($twoTimenow)) {
				//버전없이 불를 시 문제 생기는지 ? 
				//$appVersion = $this->Clientinfo->GetClientAppVersionInfo($mallId, $token->access_token);//배열로 받아오기
				// $this->session->set('app_token', $token->access_token);
				//$this->session->set_userdata('app_version', $appVersion['app']['version']);
				//엑세스 토큰이 있을 경우 토큰의 종료 시간을 비교 후 사용 가능 하면 토큰 사용 / 불가면 재톤큰으로 조회
				return $token->access_token;
				/*
				$reUrl = 'https://egongegong.kr/Admin/Index';
				header('Location: ' . $reUrl);
				*/
				//exit;
            } else {
				return $this->tokenDb->cafe24TokenIssue('r', null, $mallId, $token->refresh_token);        
            }
        } 
    }

}
