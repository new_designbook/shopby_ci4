<?php

namespace App\Controllers\Mobile;

use App\Controllers\BaseController;
use App\Models\Propose\ProposeModel;
use App\Services\ProposeService;
use App\Entities\ProposeEntities;
use App\Services\FileService;

class Propose extends BaseController
{
    public function index(string $kind = 'n')
    {
        return mobile_render('mobile/propose/index', [
            'kind' => $this->request->getVar('kind') ?? 'n',
			'data' => $this->request->getPost(),
		]);
    }

    // 생성
	public function create()
	{
        $proposeService = ProposeService::factory();
        $files = $this->request->getFiles();
        $fileSno = null;
        if(isset($files)) {
            $fileService = FileService::factory();
            $fileSno = $fileService->create($files, 'propose');
        }

		list($create_success, $sno, $errors) = 
            $proposeService
            ->create(
                array_merge($this->request->getPost(), ['file' => $fileSno])
            );
        /*
            $proposeModel = new ProposeModel();
            $sno = $proposeModel->save($this->request->getPost());
        */
            // echo $sno;
		if ($create_success){
            $data = [
               'kind' => $this->request->getVar('kind') ?? 'n'
            ];

            helper('alert');
            alert_back("저장이 완료되었습니다.");            
            return mobile_render('mobile/propose/index', $data);
		}

        $proposeEntities = new ProposeEntities();
		$proposeEntities->fill($this->request->getPost());
        
        return mobile_render('mobile/propose/index', [
            'kind' => $this->request->getVar('kind') ?? 'n',
			'data' => $proposeEntities,
			'errors' => $errors
		]);
	}

}
