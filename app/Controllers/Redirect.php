<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Cafe24Token;

class Redirect extends BaseController
{
    protected $session;
    protected $tokenDb;

    public function __construct()
    {
        //http://localhost:8080/redirect?code=sampleXeWS9W5q08ybH1XHS&state=eyJtYWxsX2lkIjoiZXRhIiwibWFsbF9jaGVjayI6ImV0YV9nYXdlZ2F3ZWdhZXdnIn0=
        $this->session = \Config\Services::session();
        $this->tokenDb = new Cafe24Token();
    }

    public function index()
    {
        $code = $this->request->getGet('code');     //2번 응답의 'code'
        $status = $this->request->getGet('state');  //2번 응답의 'state'
        $decodeStatue = json_decode(base64_decode($status), true);

        //토큰 확인 후 없으면 신규발급n, 있으면 사용u, 만료면e 재발급 분기로 구분
		$token = $this->tokenDb->GetClientTokenInfo($decodeStatue['mall_id']);//배열로 받아오기
        
        if($token) {
            //엑세스 토큰이 있을 경우 토큰의 종료 시간을 비교 후 사용 가능 하면 토큰 사용 / 불가면 재톤큰으로 조회
            $timenow = date("Y-m-d H:i:s");
            $twoTimenow = date("Y-m-d His", strtotime($timenow."+30 minutes"));

            if(strtotime($token->expires_at) >= strtotime($twoTimenow)) {
                //토큰 만료시간보다 작거나 같으면 token 값 사용
                $tokenData = $token->access_token;
            }else {
                //재발급 토큰 시간이 더 크면 재발급
                if(strtotime($token->refresh_token_expires_at) >= strtotime($twoTimenow)) {
                    //재발급
                    $tokenData = $this->tokenDb->cafe24TokenIssue('r',$code, $decodeStatue['mall_id'], $token->refresh_token);
                }else {
                    //신규발급
                    $tokenData = $this->tokenDb->cafe24TokenIssue('y',$code, $decodeStatue['mall_id']);
                }
            }
        }else {
            $tokenData = $this->tokenDb->cafe24TokenIssue('y',$code, $decodeStatue['mall_id']);
        }

		return $this->response->redirect("/admin");
		
    }

   
}
