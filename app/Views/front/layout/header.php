<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome to CodeIgniter 4!</title>
    <meta name="description" content="The small framework with powerful features">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="/favicon.ico">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/contact.css">
    
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>

    <!-- FONT -->	
	<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@300;400;500;600;700&family=Roboto:wght@100;300;400;500;700&display=swap" rel="stylesheet">

	<script language="javascript" type="text/javascript">   
		window.addEventListener('load', function() {
		  let message = {height: document.body.scrollHeight};	

		  window.top.postMessage(message, "*");
		});
	</script> 
</head>