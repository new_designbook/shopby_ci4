<div class="contact-tab">
	<ul>
		<li>
			<?php if($kind == 'n') { ?>
				<a class="active" aria-current="page" href="#">신규 입점제안</a>
			<?php } else { ?>
				<a aria-current="page" href="?kind=n">신규 입점제안</a>
			<?php } ?>
		</li>
		<li>
			<?php if($kind == 'p') { ?>
				<a class="active" aria-current="page" href="#">이공이공 파트너사</a>
			<?php } else { ?>
				<a aria-current="page" href="?kind=p">이공이공 파트너사</a>
			<?php } ?>
		</li>
	</ul>
</div>

<form method="POST" enctype="multipart/form-data">
    <input type="hidden" name="kind" value="<?=$kind; ?>" />
    <?php if($kind == 'n') { ?>
		<div class="kind-tit">
			<h2>신규 입점제안</h2>
		</div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="company" name="company" placeholder="회사명" value="<?=isset($data->company) ? $data->company : ""; ?>">
            <label for="company" class="form-label">회사명</label>
            <?php if(isset($errors['company'])) { ?>
            <div class="valid-feedback">
                <?=$errors['company']; ?>
            </div>
            <?php } ?>
        </div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="person" name="person" placeholder="담당자명" value="<?=isset($data->person) ? $data->person : ""; ?>">
            <label for="person" class="form-label">담당자명</label>
            <?php if(isset($errors['person'])) { ?>
            <div class="valid-feedback">
                <?=$errors['person']; ?>
            </div>
            <?php } ?>
        </div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="position" name="position" placeholder="직책" value="<?=isset($data->position) ? $data->position : ""; ?>">
            <label for="position" class="form-label">직책</label>
            <?php if(isset($errors['position'])) { ?>
            <div class="valid-feedback">
                <?=$errors['position']; ?>
            </div>
            <?php } ?>
        </div>
        <div class="form-floating mb-3">
            <input type="email" class="form-control" id="email" name="email" placeholder="이메일" value="<?=isset($data->email) ? $data->email : ""; ?>">
            <label for="email" class="form-label">이메일</label>
            <?php if(isset($errors['email'])) { ?>
            <div class="valid-feedback">
                <?=$errors['email']; ?>
            </div>
            <?php } ?>
        </div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="phone" name="phone" placeholder="연락처" value="<?=isset($data->phone) ? $data->phone : ""; ?>">
            <label for="phone" class="form-label">연락처</label>
            <?php if(isset($errors['phone'])) { ?>
            <div class="valid-feedback">
                <?=$errors['phone']; ?>
            </div>
            <?php } ?>
        </div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="homepage" name="homepage" placeholder="홈페이지" value="<?=isset($data->homepage) ? $data->homepage : ""; ?>">
            <label for="homepage" class="form-label">홈페이지</label>
        </div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="company_letter" name="company_letter" placeholder="회사소개서" value="<?=isset($data->company_letter) ? $data->company_letter : ""; ?>">
            <label for="company_letter" class="form-label">회사소개서</label>
        </div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="product_letter" name="product_letter" placeholder="제품소개서" value="<?=isset($data->product_letter) ? $data->product_letter : ""; ?>">
            <label for="product_letter" class="form-label">제품소개서</label>
        </div>
        <div class="form-floating mb-3">
            <textarea class="form-control" id="export_history" rows="3" placeholder="Leave a comment here" name="export_history" style="height:157px"><?=isset($data->export_history) ? ($data->export_history) : ""; ?></textarea>
            <label for="export_history" class="form-label">미국 수출 이력</label>
        </div>
        <div class="form-floating mb-3">
            <textarea class="form-control" id="etc" rows="3" placeholder="Leave a comment here" name="etc" style="height:157px"><?=isset($data->etc) ? ($data->etc) : ""; ?></textarea>
            <label for="etc" class="form-label">기타</label>
        </div>
		<!--
        <div class="mb-3">
            <label for="company_letter_file" class="form-label">회사소개서</label>
            <input type="file" name="company_letter_file" id="company_letter_file" class="form-control" >
        </div>
        <div class="mb-3">
            <label for="product_letter_file" class="form-label">미국 수출 이력</label>
            <input type="file" name="product_letter_file" id="product_letter_file" class="form-control" >
        </div>
        <div class="mb-3">
            <label for="etc_file" class="form-label">기타</label>
            <input type="file" name="etc_file" id="etc_file" class="form-control" >
        </div>-->
    <?php } else { ?>
		<div class="kind-tit">
			<h2>이공이공 파트너사</h2>
		</div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="company" name="company" placeholder="회사명" value="<?=isset($data->company) ? $data->company : ""; ?>">
            <label for="company" class="form-label">회사명</label>
            <?php if(isset($errors['company'])) { ?>
            <div class="valid-feedback">
                <?=$errors['company']; ?>
            </div>
            <?php } ?>
        </div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="person" name="person" placeholder="담당자명" value="<?=isset($data->person) ? $data->person : ""; ?>">
            <label for="person" class="form-label">담당자명</label>
            <?php if(isset($errors['person'])) { ?>
            <div class="valid-feedback">
                <?=$errors['person']; ?>
            </div>
            <?php } ?>
        </div>
        <div class="form-floating mb-3" >
            <input type="email" class="form-control" id="email" name="email" placeholder="이메일" value="<?=isset($data->email) ? $data->email : ""; ?>">
            <label for="email" class="form-label">이메일</label>
            <?php if(isset($errors['email'])) { ?>
            <div class="valid-feedback">
                <?=$errors['email']; ?>
            </div>
            <?php } ?>
        </div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="phone" name="phone" placeholder="연락처" value="<?=isset($data->phone) ? $data->phone : ""; ?>">
            <label for="phone" class="form-label">연락처</label>
            <?php if(isset($errors['phone'])) { ?>
            <div class="valid-feedback">
                <?=$errors['phone']; ?>
            </div>
            <?php } ?>
        </div>
        <div class="form-floating mb-3">
            <textarea class="form-control" id="sku" rows="3" placeholder="Leave a comment here" name="sku" style="height: 157px"><?=isset($data->sku) ? ($data->sku) : ""; ?></textarea>
            <label for="sku" class="form-label">추가 SKU 제안</label>
        </div>
        <div class="form-floating mb-3">
            <textarea class="form-control" id="etc" rows="3" placeholder="Leave a comment here" name="etc" style="height: 157px"><?=isset($data->etc) ? ($data->etc) : ""; ?></textarea>
            <label for="etc" class="form-label">기타</label>
        </div>
		<!--
        <div class="mb-3">
            <label for="sku_file" class="form-label">SKU 파일</label>
            <input type="file" name="sku_file" id="sku_file" class="form-control" >
        </div>
        <div class="mb-3">
            <label for="etc_file" class="form-label">기타</label>
            <input type="file" name="etc_file[]" id="etc_file" class="form-control" >
        </div>
        <div class="mb-3">
            <label for="etc_file" class="form-label">기타</label>
            <input type="file" name="etc_file[]" id="etc_file" class="form-control" >
        </div>-->
    <?php } ?>
    <div class="btn-box col-12">
        <button class="submit-btn" type="submit">Send</button>
    </div>
</form>
<script>
</script>