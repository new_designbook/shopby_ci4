
<div id="propose">
	<div class="inner">
		<div class="titleArea">
			<h2><?php if($kind == 'n') { echo '신규'; ?><?php } else  { echo '파트너사'; ?><?php } ?> 입점제안 리스트</h2>
		</div>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <div class="container-fluid">
			<a class="navbar-brand" href="#">MENU</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
			  <ul class="navbar-nav">
				<li class="nav-item">
					<?php if($kind == 'n') { ?>
						<a class="nav-link active" aria-current="page" href="#">신규</a>
					<?php } else { ?>
						<a class="nav-link" aria-current="page" href="?kind=n">신규</a>
					<?php } ?>
				</li>
				<li class="nav-item">
					<?php if($kind == 'p') { ?>
						<a class="nav-link active" aria-current="page" href="#">파트너사</a>
					<?php } else { ?>
						<a class="nav-link" aria-current="page" href="?kind=p">파트너사</a>
					<?php } ?>
				</li>
			  </ul>
			</div>
		  </div>
		</nav>
		<div id="propose-list">
			<form id="show_list">
				<input type="hidden" name="kind" value="<?=$kind?>" />
				<input type="hidden" name="page_propose" value="<?=$page_propose?>" />
				<div class="d-grid gap-2 d-md-flex justify-content-md-end">
					<select class="form-select change_per" name="perPage" aria-label="Default select example">
						<option value="10" <?php if($perPage == '10'): echo "selected='selected'"; endif?>>10</option>
						<option value="30" <?php if($perPage == '30'): echo "selected='selected'"; endif?>>30</option>
						<option value="50" <?php if($perPage == '50'): echo "selected='selected'"; endif?>>50</option>
					</select>
					<select class="form-select change_orderby" name="orderby" aria-label="Default select example">
						<option value="desc" <?php if($orderby == 'desc'): echo "selected='selected'"; endif?>>등록일 ↓</option>
						<option value="asc" <?php if($orderby == 'asc'): echo "selected='selected'"; endif?>>등록일 ↑</option>
					</select>
				</div>
			</form>
			<table class="table table-hover">
			  <colgroup>
			  	<col style="width:94px;">
				<col style="width:400px;">
				<col style="width:280px;">
				<col style="width:400px;">
				<col style="width:300px;">
				<col style="width:300px;">
			  </colgroup>
			  <thead>
				<th>번호</th>
				<th>회사명</th>
				<th>담당자명</th>
				<th>이메일</th>
				<th>등록일</th>
				<th>상세</th>
			  </thead>
			  <tbody>
				<?php 
				foreach ($propose_list as $data) :
					?>
					<tr>
						<td><?=$idx--?></td>
						<td>
						  <?php if(isset($data->file) && $data->file != ''): ?>
							<span>[첨부파일](<?=count(explode(',', $data->file));?></spna>)
						  <?php endif; ?>
						  <?=$data->company ?>
						</td>
						<td><?=$data->person ?></td>
						<td><?=$data->email ?></td>
						<td><?=$data->created_at ?></td>
						<td>
						  <a href="<?= site_url("/admin/propose/show/{$data->sno}?".$queryParam) ?>">
							<button type="button" class="btn btn-primary">상세
							</button>
						  </a>
						</td>
					</tr>
				<?php endforeach; ?>
			  </tbody>
			</table>
		</div>
	</div>
</div>
<?= $pager->links('propose','cus_pager') ?>

<script>
    const selectElement = document.querySelector(".change_per");

    selectElement.addEventListener("change", (event) => {
		$('input[name=\'page_propose\']').val(1);
        document.getElementById('show_list').submit();
    });
    
    const selectElement2 = document.querySelector(".change_orderby");

    selectElement2.addEventListener("change", (event) => {
        document.getElementById('show_list').submit();
    });
</script>