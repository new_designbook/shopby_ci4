<div id="propose" class="propose-view">
	<div class="inner">
		<div class="titleArea">
			<h2>상세페이지</h2>
		</div>
		<nav class="navbar navbar-expand-lg bg-body-tertiary bg-light">
		  <div class="container-fluid">
			<a class="navbar-brand" href="#">MENU</a>
			<div class="collapse navbar-collapse" id="navbarNav">
			  <ul class="navbar-nav">
				<li class="nav-item">
					<?php if($propose['kind'] == 'n') { ?>
						<a class="nav-link active" aria-current="page" href="<?= site_url("/admin/propose?kind=n") ?>">신규</a>
					<?php } else { ?>
						<a class="nav-link" aria-current="page" href="<?= site_url("/admin/propose?kind=n") ?>">신규</a>
					<?php } ?>
				</li>
				<li class="nav-item">
					<?php if($propose['kind'] == 'p') { ?>
						<a class="nav-link active" aria-current="page" href="<?= site_url("/admin/propose?kind=p") ?>">파트너사</a>
					<?php } else { ?>
						<a class="nav-link" aria-current="page" href="<?= site_url("/admin/propose?kind=p") ?>">파트너사</a>
					<?php } ?>
				</li>
			  </ul>
			</div>
		  </div>
		</nav>

		<form method="POST" id="propose_update" action="<?= site_url("/admin/propose/edit/".$propose['sno'] ) ?>" >
			<input type="hidden" name="kind" value="<?=$propose['kind']; ?>" />
			<?php if($propose['kind'] == 'n') { ?>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="company" name="company" value="<?=$propose['company']?>" placeholder="회사명" readonly>
					<label for="company" class="form-label">회사명</label>
				</div>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="person" name="person" value="<?=$propose['person']?>"  placeholder="담당자명" readonly>
					<label for="person" class="form-label">담당자명</label>
				</div>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="position" name="position" value="<?=$propose['position']?>"  placeholder="직책" readonly>
					<label for="position" class="form-label">직책</label>
				</div>
				<div class="form-floating mb-3">
					<input type="email" class="form-control" id="email" name="email" value="<?=$propose['email']?>"  placeholder="이메일" readonly>
					<label for="email" class="form-label">이메일</label>
				</div>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="phone" name="phone"  value="<?=$propose['phone']?>" placeholder="연락처" readonly>
					<label for="phone" class="form-label">연락처</label>
				</div>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="homepage" name="homepage"  value="<?=$propose['homepage']?>" placeholder="홈페이지" readonly>
					<label for="homepage" class="form-label">홈페이지</label>
				</div>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="company_letter" name="company_letter" value="<?=$propose['company_letter']?>"  placeholder="회사소개서" readonly>
					<label for="company_letter" class="form-label">회사소개서</label>
				</div>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="product_letter" name="product_letter" value="<?=$propose['product_letter']?>"  placeholder="제품소개서" readonly>
					<label for="product_letter" class="form-label">제품소개서</label>
				</div>
				<div class="form-floating">
					<textarea class="form-control" id="export_history" rows="3" placeholder="Leave a comment here" name="export_history" style="height: 100px"><?=$propose['export_history'];?></textarea>
					<label for="export_history" class="form-label">미국 수출 이력</label>
				</div>
				<div class="form-floating">
					<textarea class="form-control" id="etc" rows="3" placeholder="Leave a comment here" name="etc" style="height: 100px"><?=$propose['etc'];?></textarea>
					<label for="etc" class="form-label">기타</label>
				</div>
			<?php } else { ?>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="company" name="company" value="<?=$propose['company']?>"  placeholder="회사명" readonly>
					<label for="company" class="form-label">회사명</label>
				</div>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="person" name="person" value="<?=$propose['person']?>"  placeholder="담당자명" readonly>
					<label for="person" class="form-label">담당자명</label>
				</div>
				<div class="form-floating mb-3" >
					<input type="email" class="form-control" id="email" name="email"  value="<?=$propose['email']?>" placeholder="이메일" readonly>
					<label for="email" class="form-label">이메일</label>
				</div>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="phone" name="phone" value="<?=$propose['phone']?>"  placeholder="연락처" readonly>
					<label for="phone" class="form-label">연락처</label>
				</div>
				<div class="form-floating">
					<textarea class="form-control" id="sku" rows="3" placeholder="Leave a comment here" name="sku" style="height: 100px"><?=$propose['sku'];?></textarea>
					<label for="sku" class="form-label">추가 SKU 제안</label>
				</div>
				<div class="form-floating">
					<textarea class="form-control" id="etc" rows="3" placeholder="Leave a comment here" name="etc" style="height: 100px"><?=$propose['etc'];?></textarea>
					<label for="etc" class="form-label">기타</label>
				</div>
			<?php } ?>
		</form>
		<div class="d-grid gap-2 d-md-flex justify-content-md-end">
			<a href="<?= site_url("/admin/propose?".$queryParam) ?>">
				<button type="button" class="btn btn-primary">목록
				</button>
			</a>
			<button type="button" class="btn btn-success " id="propose_update_btn">수정
			</button>
			<form method="POST" action="<?= site_url("/admin/propose/delete") ?>" >
				<input type="hidden" name="sno" value="<?=$propose['sno']; ?>" />
				<input type="hidden" name="file" value="<?=$propose['file']; ?>" />
				<button type="submit" class="btn btn-danger">삭제
				</button>
			</form>
		</div>
	</div>
</div>
<?php if(isset($propose['fileInfo']['src'])):?>
<div class="row row-cols-1 row-cols-md-5 g-4">
	<?php foreach($propose['fileInfo']['src'] as $name => $files) : ?>
		<?php foreach($files as $key => $file) : ?>
			<div class="col">
				<div class="card" style="width: 18rem;">
					<?php if($propose['fileInfo']['imageFl'][$name][$key] == 'y'): ?>
					<img src="<?= base_url('../uploads/' .$file )?>"  class="card-img-top" alt="<?=$name; ?>">
					<?php else: ?>
					<p>이미지 파일이 아닙니다.</p>
					<?php endif; ?>
					<div class="card-body">
						<p class="card-text"><a href="<?= base_url('../uploads/' .$file )?>" download="<?=$propose['fileInfo']['name'][$name][$key]?>"><?=$propose['fileInfo']['name'][$name][$key]?></a></p>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php endforeach; ?>
</div>



<?php endif; ?>
<script>
    const element = document.getElementById("propose_update_btn");
    element.addEventListener("click", (event) => {
        document.getElementById('propose_update').submit();
    });

    document.addEventListener("DOMContentLoaded", () => {
        // 파일 다운로드
        /*
        document.getElementsByClassName("file_down").addEventListener("click", getFileDownload);
        
        $('.file_down').on('click', () => {
            const sno = $(this).data('sno');
            $.ajax({
                url: '/admin/propose/down/'+sno
            }).done((data) => {
                alert('다운이 완료되었습니다.')
            });
        })
        */
    })
    function getFileDownload(sno) {
        //XMLHttpRequest 객체 생성
        var xhr = new XMLHttpRequest();

        //요청을 보낼 방식, 주소, 비동기여부 설정
        xhr.open('GET', "/admin/propose/down/"+sno, true);
        xhr.responseType = "blob";
        xhr.setRequestHeader('my-custom-header', 'custom-value'); 

        //요청 전송
        xhr.send();

        //통신후 작업
        xhr.onload = () => {
            //통신 성공
            if (xhr.status == 200) {
                console.log(xhr.response);
                console.log("통신 성공");
            } else {
                //통신 실패
                console.log("통신 실패");
            }
        }
    }
</script>