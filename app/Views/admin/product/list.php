
<div id="propose">
	<div class="inner">
		<div class="titleArea">
			<h2>상품 리스트</h2>
		</div>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <div class="container-fluid">
			<a class="navbar-brand" href="#">MENU</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
			  <ul class="navbar-nav">
				<li class="nav-item">
					
				</li>
				<li class="nav-item">
					
				</li>
			  </ul>
			</div>
		  </div>
		</nav>
		<div id="propose-list">
			<form id="show_list">
				<div class="d-grid gap-2 d-md-flex justify-content-md-end">
					<select class="form-select change_per" name="perPage" aria-label="Default select example">
						<option value="10" <?php if($perPage == '10'): echo "selected='selected'"; endif?>>10</option>
						<option value="30" <?php if($perPage == '30'): echo "selected='selected'"; endif?>>30</option>
						<option value="50" <?php if($perPage == '50'): echo "selected='selected'"; endif?>>50</option>
					</select>
					<select class="form-select change_orderby" name="orderby" aria-label="Default select example" style="display:none">
						<option value="desc" <?php if($orderby == 'desc'): echo "selected='selected'"; endif?>>등록일 ↓</option>
						<option value="asc" <?php if($orderby == 'asc'): echo "selected='selected'"; endif?>>등록일 ↑</option>
					</select>
				</div>
			</form>
			<table class="table table-hover">
			  <colgroup>
			  	<col style="width:94px;">
				<col style="width:280px;">
				<col style="width:400px;">
			  </colgroup>
			  <thead>
				<th>수정</th>
				<th>상품번호</th>
				<th>상품명</th>
			  </thead>
			  <tbody>
				<?php foreach ($list['elements'] as $data) : ?>
					<tr>
						<td>
							<a href="./product/show/<?= $data['mallProductNo']?>?page=<?= $page?>">수정</a>
						</td>
						<td>
							<?= $data['mallProductNo']?>
						</td>
						<td>
							<?= $data['productName']?>
						</td>
					</tr>
				<?php endforeach; ?>
			  </tbody>
			</table>
		</div>
	</div>
</div>
<?= $pager_links ?>

<script>
    const selectElement2 = document.querySelector(".change_orderby");

    selectElement2.addEventListener("change", (event) => {
        document.getElementById('show_list').submit();
    });
</script>