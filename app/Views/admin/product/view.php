
<form method="POST" enctype="" action="/admin/product/edit">
    <input type="hidden" name="product_no" value="<?= $product_no ?>">
    <input type="hidden" name="page" value="<?= $page ?>">
		<div class="kind-tit">
			<h2>리스트_동영상정보</h2>
		</div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="list_movie_link" name="list_movie_link" placeholder="동영상정보" value="<?= isset($data->list_movie_link)? $data->list_movie_link:''; ?>">
            <label for="list_movie_link" class="form-label">URL</label>
        </div>
       
        <div class="kind-tit">
			<h2>상세_동영상정보</h2>
		</div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="detail_movie_link_1" name="detail_movie_link_1" placeholder="동영상정보" value="<?= isset($data->detail_movie_link_1)? $data->detail_movie_link_1:''; ?>">
            <label for="detail_movie_link_1" class="form-label">URL</label>
        </div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="detail_movie_link_2" name="detail_movie_link_2" placeholder="동영상정보" value="<?= isset($data->detail_movie_link_2)? $data->detail_movie_link_2:''; ?>">
            <label for="detail_movie_link_2" class="form-label">URL</label>
        </div>
    <div class="btn-box col-12">
		<button class="list-btn" type="button">목록</button>
        <button class="submit-btn" type="submit">저장</button>
    </div>
</form>
<script>
	$(document).ready(function() {
		$('.list-btn').click(function() {
			location.href="/admin/product?page=<?=$page?>";
		})
	});
</script>