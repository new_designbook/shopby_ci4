<style>
	
</style>

<div id="script">
	<div class="inner">
		<div class="titleArea">
			<h2>스크립트 추가/삭제</h2>
		</div>
		<div class="script-switch">
			<input type="checkbox" id="toggle" hidden <?php if($script) echo 'checked'; ?> >
			<div id="toggleContents" class="text-code"> [상태] <?php if($script) { echo '스크립트 추가됨'; } else { echo '스크립트 삭제됨';} ?></div>

			<label for="toggle" class="toggleSwitch">
				<span class="toggleButton"></span>
			</label>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
	var flag = true;
	$('#toggle').on('click', function(){
		if(flag) {
			flag = false;
			if('<?=$script?>' != '0'){	//스크립트 삭제
				var url = "/admin/script/<?=$script?>";
				var type = 'PUT';
			} else {	// 스트립트 추가
				var url = "/admin/script";
				var type = 'POST';
			}
			$.ajax({
				type: type,
				url: url,
				success: function(result){
					// console.log(result)
					location.reload();
				}
			});
		}
	});
});
</script>

