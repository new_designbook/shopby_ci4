
<form method="POST" enctype="" action="/admin/movie/edit">
    <input type="hidden" name="sno" value="<?= $data->sno ?>">
		<div class="kind-tit">
			<h2>PC_동영상정보</h2>
		</div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="pc_movie_url_1" name="pc_movie_url_1" placeholder="동영상정보" value="<?= $data->pc_movie_url_1 ?>">
            <label for="pc_movie_url_1" class="form-label">URL</label>
        </div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="pc_movie_url_2" name="pc_movie_url_2" placeholder="동영상정보" value="<?= $data->pc_movie_url_2 ?>">
            <label for="pc_movie_url_2" class="form-label">URL</label>
        </div>
        <div class="kind-tit">
			<h2>Mobile_동영상정보</h2>
		</div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="mo_movie_url_1" name="mo_movie_url_1" placeholder="동영상정보" value="<?= $data->mo_movie_url_1 ?>">
            <label for="mo_movie_url_1" class="form-label">URL</label>
        </div>
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="mo_movie_url_2" name="mo_movie_url_2" placeholder="동영상정보" value="<?= $data->mo_movie_url_2 ?>">
            <label for="mo_movie_url_2" class="form-label">URL</label>
        </div>
    <div class="btn-box col-12">
        <button class="submit-btn" type="submit">저장</button>
    </div>
</form>
<script>
</script>