<div id="icon" class="icon-view">
	<div class="inner">
		<div class="titleArea">
			<h2>특정 쿠폰 설정</h2>
		</div>
		<div class="alert alert-danger">
			<strong>해당 아이콘은 콤마를 기준으로 입력해야 합니다.<br/>(예: Wispy,Mix)</strong>
		</div>
		<form method="POST" id="icon_update" action="/admin/icon/edit" >
			<input type="hidden" name="sno" value="<?=$icon['sno']; ?>" />
			<?php foreach($keyArray as $key => $val) { ?>
			<div class="form-floating mb-3">
				<input type="text" class="form-control" id="icon<?=$val; ?>" name="icon<?=$val; ?>" value="<?=$icon['icon'.$val];?>"  placeholder="<?=$val?>번째 아이콘">
				<label for="icon<?=$val; ?>" class="form-label"><?=$val?>번째 아이콘</label>
			</div>
			<?php } ?>
		</form>
		<div class="d-grid gap-2 d-md-flex justify-content-md-end">
			<button type="button" class="btn btn-success " id="icon_update_btn">저장
			</button>
		</div>
	</div>
</div>

<script>
    const element = document.getElementById("icon_update_btn");
    element.addEventListener("click", (event) => {
        document.getElementById('icon_update').submit();
    });
</script>