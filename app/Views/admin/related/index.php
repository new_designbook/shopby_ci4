
<div id="related">
	<form id="show_list">
		<div class="inner">
			<div class="titleArea">
				<h2>상품 선택</h2>
			</div>
			<div id="related-list">
				<div>
					<input type="hidden" name="page" id="page" value="<?=$req['pageNumber']?>" />
					<input type="hidden" name="pageSize" value="<?=$req['pageSize']?>" />
					검색어 : <input type="text" name="filter[keywords]" class="from-control" value="<?=$req['filter.keywords']; ?>" />
				</div>
				<div>
					판매상태
					<select name="filter.saleStatus">
						<?php foreach($selectData['saleStatus'] as $key => $val) { ?>
							<option value="<?=$key; ?>" <?php if($req['filter.saleStatus'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					품절상품포함여부
					<select name="filter.soldout">
						<?php foreach($selectData['soldout'] as $key => $val) { ?>
							<option value="<?=$key; ?>" <?php if($req['filter.soldout'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					<select name="order.by">
						<?php foreach($selectData['by'] as $key => $val) { ?>
							<option value="<?=$key; ?>" <?php if($req['order.by'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					<select name="order.direction">
						<?php foreach($selectData['direction'] as $key => $val) { ?>
							<option value="<?=$key; ?>" <?php if($req['order.direction'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					<select name="order.soldoutPlaceEnd">
						<?php foreach($selectData['soldoutPlaceEnd'] as $key => $val) { ?>
							<option value="<?=$key; ?>" <?php if($req['order.soldoutPlaceEnd'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
						<?php } ?>
					</select>
				</div>
				<button type="button" id="formSubmit">검색</button>
			</div>
		</div>
		<div class="d-md-flex" style="gap:40px;">
			<div>
				총 <?=$totalCount; ?>개
				<h1>상품리스트</h1>
				<table class="table table-hover">
					<colgroup>
					<col style="width:94px;">
					<col style="width:280px;">
					<col style="width:400px;">
					<col style="width:280px;">
					<col style="width:400px;">
					<col style="width:300px;">
					<col style="width:300px;">
					</colgroup>
					<thead>
					<th>
						<input type="checkbox" class="form-checkbox" id="checkAll">
					</th>
					<th>이미지</th>
					<th>상품명</th>
					<th>공급사</th>
					<th>판매가</th>
					<th>재고</th>
					<th>품절여부</th>
					</thead>
					<tbody id="searchProduct">
					<?php 
					if($product_list) {
						foreach ($product_list as $data) :
							if(isset($data['isSoldOut']) && $data['isSoldOut'] != '') {
								$soldOut = '품절';
							} else {
								$soldOut = '정상';
							}
						?>
						<tr id="tbl_add_goods_<?= $data['productNo'];?>" class="add_goods_free" data-key="<?= $data['productNo'];?>">
							<td>
								<input type="checkbox" value="<?=$data['productNo']; ?>" class="form-checkbox dbk_checked" id="dbk_tr_<?= $data['productNo'];?>" data-img="<?=$data['listImageUrls'][0]?>" 
									data-name="<?=$data['productName']; ?>" data-stock="<?=number_format($data['stockCnt']); ?>" data-soldout="<?=$soldOut;?>"
									data-partner="<?=$data['partnerName']; ?>" data-price="<?=number_format($data['salePrice']); ?>"
								/>
							</td>
							<td>
								<img src="<?=$data['listImageUrls'][0]?>" style="width:150px;"/>
							</td>
							<td>
								<span><?=$data['productName']; ?></span>
							</td>
							<td>
								<span><?=$data['partnerName']; ?></span>
							</td>
							<td>
								<?=number_format($data['salePrice']); ?>
							</td>
							<td>
								<?=number_format($data['stockCnt']); ?>
							</td>
							<td>
								<?=$soldOut;?>
							</td>
						</tr>
					<?php endforeach; 
					} else { ?>
						<tr><td colspan="7">검색된 상품이 없습니다.</td></tr>
					<?php } ?>
					</tbody>
				</table>
				<?= $pager_links ?>
			</div>
			<div class="d-grid gap-2 d-md-flex justify-content-md-center" style="flex-direction:column;">
				<button type="button" class="btn btn-success" id="update_btn">추가하기</button>
				<button type="button" class="btn btn-success" id="delete_btn">삭제하기</button>
				<button type="button" class="btn btn-success" id="insert_btn">저장하기</button>
			</div>
			<div>
				<h1>선택된 상품</h1>
				<table class="table table-hover">
					<colgroup>
					<col style="width:94px;">
					<col style="width:280px;">
					<col style="width:400px;">
					<col style="width:280px;">
					<col style="width:400px;">
					<col style="width:300px;">
					<col style="width:300px;">
					</colgroup>
					<thead>
					<th>
						<input type="checkbox" class="form-checkbox" id="checkAddAll">
					</th>
					<th>이미지</th>
					<th>상품명</th>
					<th>공급사</th>
					<th>판매가</th>
					<th>재고</th>
					<th>품절여부</th>
					</thead>
					<tbody id="addProduct">
					<?php 
					if(isset($subProductList)) {
						foreach ($subProductList as $data) :
							if(isset($data['isSoldOut']) && $data['isSoldOut'] != '') {
								$soldOut = '품절';
							} else {
								$soldOut = '정상';
							}
						?>
						<tr id="tbl_add_goods_<?= $data['productNo'];?>" class="dbk_add_goods_free move-row" data-key="<?= $data['productNo'];?>">
							<td>
								<input type="hidden" name="productNo[]" value="<?=$data['productNo']?>" />
								<input type="checkbox" value="<?=$data['productNo']; ?>" class="form-checkbox dbk_add_checked" id="dbk_add_tr_<?= $data['productNo'];?>"/>
							</td>
							<td>
								<img src="<?=$data['listImageUrls'][0]?>" style="width:150px;"/>
							</td>
							<td>
								<span><?=$data['productName']; ?></span>
							</td>
							<td>
								<span><?=$data['partnerName']; ?></span>
							</td>
							<td>
								<?=number_format($data['salePrice']); ?>
							</td>
							<td>
								<?=number_format($data['stockCnt']); ?>
							</td>
							<td>
								<?=$soldOut; ?>
							</td>
						</tr>
					<?php endforeach; 
					} ?>
					</tbody>
				</table>
			</div>
		</div>
	</form>
</div>

<script>
	function getList(page) {
		$('#page').val(page);
		$('#show_list').submit();
	}

	$(document).ready(function(){
		<?php if(isset($req)) { ?>
			const query = "<?=http_build_query($req);?>";
			$('.pagination > li > a').each(function(){
				// $(this).prop("href", $(this).prop("href")+"&"+ query);
				$(this).prop("href", "#");
				const page = $(this).html().trim();
				$(this).on('click', function(){
					getList(page);
				});
			});
		<?php } ?>

		// 검색 버튼
		$('#formSubmit').on('click', function(){
			$('#show_list').submit();
		});

		// 채크 박스 클릭 좌
		$('.add_goods_free').on('click', function(){
			const key = $(this).data('key');
			$("#dbk_tr_"+key).prop("checked", !$("#dbk_tr_"+key).prop("checked"));
			// $("#dbk_tr_"+key).trigger('click');
			$('#checkAll').prop('checked', ($('.dbk_checked:checked').length == $('.dbk_checked').length));
		});

		// 채크 박스 클릭 우
		$(document).on('click', '.dbk_add_goods_free', function(){
			$(this).find('.dbk_add_checked').prop("checked", !$(this).find('.dbk_add_checked').prop("checked"));
			// $(this).find('.dbk_add_checked').trigger('click');
			$('#checkAddAll').prop('checked', ($('.dbk_add_checked:checked').length == $('.dbk_add_checked').length));
		});

		// 전체 클릭 
		$('#checkAll').on('click', function() {
			const checkedFlag = $(this).prop('checked');
			$('.dbk_checked').prop('checked', checkedFlag);
		});

		// 전체 클릭 
		$('#checkAddAll').on('click', function() {
			const checkedFlag = $(this).prop('checked');
			$('.dbk_add_checked').prop('checked', checkedFlag);
		});

		// 좌에서 우로
		$('#update_btn').on('click', function() {
			if($('.dbk_checked:checked').length<1) {
				alert('상품을 선택해주세요.');
				return;
			} else {
				console.log('click');
				$('.dbk_checked:checked').each(function() {
					const productNo = $(this).val();
					if($('#dbk_add_tr_'+productNo).length != 0) {
						// 이때는 추가 안함
					} else {
						const img = $(this).data('img');
						const productName = $(this).data('name');
						const stock = $(this).data('stock');
						const soldout = $(this).data('soldout');
						const partnerName = $(this).data('partner');
						const price = $(this).data('price');
						const $tr = $('<tr/>', {class:'dbk_add_goods_free'});
						const $input = $('<input/>', {type:'hidden', name:'productNo[]', value:productNo });
						const $checkbox = $('<input/>', {type:'checkbox', class:'form-checkbox dbk_add_checked', id:'dbk_add_tr_'+productNo, value:productNo });
						const $img = $('<img/>', {src:img, style:'width:150px;' });
						$tr.append($('<td/>').append($input).append($checkbox));
						$tr.append($('<td/>').append($img));
						$tr.append($('<td/>').html(productName));
						$tr.append($('<td/>').html(partnerName));
						$tr.append($('<td/>').html(price));
						$tr.append($('<td/>').html(stock));
						$tr.append($('<td/>').html(soldout));
						$('#addProduct').append($tr);
						$(this).closest('tr').remove();
					}
				});
			}
		});

		// 우 아이템 삭제
		$('#delete_btn').on('click', function(){
			if($('.dbk_add_checked:checked').length<1) {
				alert('상품을 선택해주세요.');
				return;
			} else {
				$('.dbk_add_checked:checked').each(function(){
					$(this).closest('tr').remove();
				});
			}
		});

		// 저장하기
		$(document).on('click', '#insert_btn', function(){
			console.log('click...')
			// 여기서 부모창에 상품번호들을 넘겨줘야함 
			if($('.dbk_add_checked').length<1) {
				// alert('상품을 선택해주세요.');
				// alert('선택한 상품이 없습니다.적용되었습니다. 저장 후 반영됩니다.');
				window.close();
				return;
			} else {
				var productNos = new Array();
				$('.dbk_add_checked').each(function() {
					productNos.push($(this).val());
				});
				const data = $('#addProduct').html();
				
				$(opener.document).find('#dbk_product').val(productNos.join('^|^'));
				$(opener.document).find('#dbk_addProduct').empty().html(data);
				alert('상품이 적용되었습니다. 저장 후 반영됩니다.');
				window.close();
			}
		});
	});
</script>