
<div id="related">
	<div class="inner">
		<div class="titleArea">
			<h2>상품 선택</h2>
		</div>
		<div class="d-grid gap-2 d-md-flex justify-content-md-end">
			<button type="button" class="btn btn-success " id="related_btn">상품 등록</button>
		</div>
		<div id="related-list">
			<form id="show_list">
				<div>
					검색어 : <input type="text" name="filter[keyword]" class="from-control" value="<?=$req['filter']['keyword']; ?>" />
				</div>
				<div>
					판매상태
					<select name="filter[saleStatus]">
						<?php foreach($selectData['saleStatus'] as $key => $val) { ?>
							<option value="<?=$key; ?>" <?php if($req['filter']['saleStatus'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					품절상품포함여부
					<select name="filter[soldout]">
						<?php foreach($selectData['soldout'] as $key => $val) { ?>
							<option value="<?=$key; ?>" <?php if($req['filter']['soldout'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					<select name="order[by]">
						<?php foreach($selectData['by'] as $key => $val) { ?>
							<option value="<?=$key; ?>" <?php if($req['order']['by'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					<select name="order[direction]">
						<?php foreach($selectData['direction'] as $key => $val) { ?>
							<option value="<?=$key; ?>" <?php if($req['order']['direction'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					<select name="order[soldoutPlaceEnd]">
						<?php foreach($selectData['soldoutPlaceEnd'] as $key => $val) { ?>
							<option value="<?=$key; ?>" <?php if($req['order']['soldoutPlaceEnd'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
						<?php } ?>
					</select>
				</div>
				<button type="button" id="formSubmit">검색</button>
			</form>
		</div>
	</div>
	<div>
		총 <?=$totalCount; ?>개
		<h1>상품리스트</h1>
		<table class="table table-hover">
			<colgroup>
			<col style="width:94px;">
			<col style="width:280px;">
			<col style="width:400px;">
			<col style="width:280px;">
			<col style="width:400px;">
			<col style="width:300px;">
			<col style="width:300px;">
			</colgroup>
			<thead>
			<th>
				<input type="checkbox" class="form-checkbox" id="checkAll">
			</th>
			<th>이미지</th>
			<th>상품명</th>
			<th>판매가</th>
			<th>공급사</th>
			<th>재고</th>
			<th>품절여부</th>
			</thead>
			<tbody>
			<?php 
			if($product_list) {
				foreach ($product_list as $data) :
				?>
				<tr id="tbl_add_goods_<?= $data['productNo'];?>" class="add_goods_free">
					<td>
						<input type="checkbox" name="productNo[]" value="<?=$data['productNo']; ?>" class="form-checkbox dbk_checked" data-sno=""/>
					</td>
					<td>
						<img src="<?=$data['listImageUrls'][0]?>" style="width:150px;"/>
					</td>
					<td>
						<span><?=$data['productName']; ?></span>
					</td>
					<td>
						<span><?=$data['partnerName']; ?></span>
					</td>
					<td>
						<?=number_format($data['salePrice']); ?>
					</td>
					<td>
						<?=number_format($data['stockCnt']); ?>
					</td>
					<td>
						<?php if(isset($data['isSoldOut']) && $data['isSoldOut'] != '') { ?>
							품절
						<?php } else { ?>
							정상
						<?php } ?>
					</td>
				</tr>
			<?php endforeach; 
			} else { ?>
				<tr><td colspan="7">검색된 상품이 없습니다.</td></tr>
			<?php } ?>
			</tbody>
		</table>
		<?= $pager_links ?>
	</div>
	
	<div>
		<h1>선택된 상품</h1>
		<?php 
		?>
	</div>
</div>

<script>
	$(document).ready(function(){
		<?php if(isset($req)) { ?>
			const query = "<?=http_build_query($req);?>";
			$('.pagination > li > a').each(function(){
				console.log($(this).prop("href"))
				$(this).prop("href", $(this).prop("href")+"&"+ query);
			})
		<?php } ?>

		// 검색 버튼
		$('#formSubmit').on('click', function(){
			$('#show_list').submit();
		});
	});
</script>