
<table class="table table-hover">
    <colgroup>
    <col style="width:94px;">
    <col style="width:280px;">
    <col style="width:400px;">
    <col style="width:280px;">
    <col style="width:400px;">
    <col style="width:300px;">
    <col style="width:300px;">
    </colgroup>
    <thead>
    <th>
        <input type="checkbox" class="form-control" id="checkAll">
    </th>
    <th>이미지</th>
    <th>상품명</th>
    <th>판매가</th>
    <th>공급사</th>
    <th>재고</th>
    <th>품절여부</th>
    </thead>
    <tbody>
    <?php 
    if($product_list) {
        foreach ($product_list as $data) :
        ?>
        <tr id="tbl_add_goods_<?= $data['productNo'];?>" class="add_goods_free">
            <td>
                <input type="hidden" name="productNo[]" value="<?=$data['productNo']; ?>" class="form-control dbk_checked" data-sno=""/>
            </td>
            <td>
                <img src="<?=$data['listImageUrls'][0]?>" style="width:150px;"/>
            </td>
            <td>
                <span><?=$data['productName']; ?></span>
            </td>
            <td>
                <?=money_format($data['salePrice']); ?>
            </td>
            <td>
                <?=money_format($data['stockCnt']); ?>
            </td>
            <td>
                <?php if(isset($data['isSoldOut'])) { ?>
                    품절
                <?php } else { ?>
                    정상
                <?php } ?>
            </td>
        </tr>
    <?php endforeach; 
    } else { ?>
        <tr><td colspan="7">검색된 상품이 없습니다.</td></tr>
    <?php } ?>
    </tbody>
</table>