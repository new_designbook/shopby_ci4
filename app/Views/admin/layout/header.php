<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">

	<link type="text/css" rel="stylesheet" href="/css/_dbook.css" />
 
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script src="/js/bootstrap-datepicker.min.js"></script>
    <script src="/js/bootstrap-datepicker.ko.min.js"></script>

	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@300;400;500;600;700&family=Roboto:wght@100;300;400;500;700&display=swap" rel="stylesheet">

    <title>[디자인교과서]올리오앱</title>
  </head>
  <body>
	<header>
		<div class="header-inner">
			<h1 class="top-logo ">
				<span class="logo"><img src="/uploads/logo.png" alt=""></span>
			</h1>
			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link <?php if(url_is('/admin/product*')) { echo 'active';} ?>" aria-current="page" href="<?= "/admin/product" ?>">상품동영상 관리</a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?php if(url_is('/admin/movie*')) { echo 'active';} ?>" aria-current="page" href="<?= "/admin/movie" ?>">메인동영상 관리</a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?php if(url_is('/admin/promostion*')) { echo 'active';} ?>" aria-current="page" href="<?= "/admin/promostion" ?>">메거진 관리</a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?php if(url_is('/admin/coupon*')) { echo 'active';} ?>" aria-current="page" href="<?= "/admin/coupon" ?>">특정쿠폰 관리</a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?php if(url_is('/admin/icon*')) { echo 'active';} ?>" aria-current="page" href="<?= "/admin/icon" ?>">번들상품 아이콘 관리</a>
				</li>
			</ul>
		</div>
	</header>