
<div id="promostion">
	<div class="inner">
		<div class="titleArea">
			<h2>메거진 리스트</h2>
		</div>
		<div class="d-grid gap-2 d-md-flex justify-content-md-end">
			<button type="button" class="btn btn-success " id="promostion_btn">메거진 등록</button>
		</div>
		<div id="promostion-list">
			<form id="show_list">
				<input type="hidden" name="page_promostion" value="<?=$page_promostion?>" />
				<div class="d-grid gap-2 d-md-flex justify-content-md-end">
					<select class="form-select change_per" name="perPage" aria-label="Default select example">
						<option value="10" <?php if($perPage == '10'): echo "selected='selected'"; endif?>>10</option>
						<option value="30" <?php if($perPage == '30'): echo "selected='selected'"; endif?>>30</option>
						<option value="50" <?php if($perPage == '50'): echo "selected='selected'"; endif?>>50</option>
					</select>
					<select class="form-select change_orderby" name="orderby" aria-label="Default select example">
						<option value="desc" <?php if($orderby == 'desc'): echo "selected='selected'"; endif?>>등록일 ↓</option>
						<option value="asc" <?php if($orderby == 'asc'): echo "selected='selected'"; endif?>>등록일 ↑</option>
					</select>
				</div>
			</form>
			<table class="table table-hover">
			  <colgroup>
			  	<col style="width:94px;">
				<col style="width:400px;">
				<col style="width:280px;">
				<col style="width:400px;">
				<col style="width:300px;">
				<col style="width:300px;">
				<col style="width:300px;">
			  </colgroup>
			  <thead>
				<th>번호</th>
				<th>메거진 제목</th>
				<th>썸네일PC</th>
				<th>썸네일MO</th>
				<th>진행기간</th>
				<th>등록일</th>
				<th>상세</th>
			  </thead>
			  <tbody>
				<?php 
				if($promostion_list) {
					foreach ($promostion_list as $data) :
					?>
					<tr>
						<td><?=$idx--?></td>
						<td>
							[<?=$data->status ?>]
						  	<?=$data->eventName ?>
						</td>
						<td>
							<?php if($data->eventThumbnail) { ?>
								<img src="<?= base_url('../uploads/' .$data->eventThumbnail )?>" style="width:150px;"/>
							<?php } else { ?>
								-
							<?php } ?>
						</td>
						<td>
							<?php if($data->eventThumbnailMobile) { ?>
								<img src="<?= base_url('../uploads/' .$data->eventThumbnailMobile )?>" style="width:150px;"/>
							<?php } else { ?>
								-
							<?php } ?>
						</td>
						<td><?=$data->eventStart ?> ~ <?=$data->eventEnd ?></td>
						<td><?=$data->created_at ?></td>
						<td>
						  <a href="<?= "/admin/promostion/show/{$data->sno}?".$queryParam ?>">
							<button type="button" class="btn btn-primary">상세
							</button>
						  </a>
						</td>
					</tr>
				<?php endforeach; 
				} else { ?>
					<tr><td colspan="7">등록된 기획전이 없습니다.</td></tr>
				<?php } ?>
			  </tbody>
			</table>
		</div>
	</div>
</div>
<?= $pager->links('promostion','cus_pager') ?>

<script>
    const selectElement = document.querySelector(".change_per");

    selectElement.addEventListener("change", (event) => {
		$('input[name=\'page_promostion\']').val(1);
        document.getElementById('show_list').submit();
    });
    
    const selectElement2 = document.querySelector(".change_orderby");

    selectElement2.addEventListener("change", (event) => {
        document.getElementById('show_list').submit();
    });

    const promostionBtn = document.getElementById("promostion_btn");

    promostionBtn.addEventListener("click", (event) => {
		location.href = "/admin/promostion/show/0?<?=$queryParam?>";
    });
</script>