<style>
.table-hover>tbody>tr.warning>* { background: #fcf8e3; }
.ck-editor__editable { 
	min-height: 250px;
}
.table-condensed > tbody > tr > td:first-child { background-color:#f00; }
</style>
<!--script src="/js/ckeditor.js"></script-->

<script src="/js/ckeditor/build/ckeditor.js"></script>

<div id="promostion" class="promostion-view">
	<div class="inner">
		<div class="titleArea">
			<h2>메거진 <?=$title; ?></h2>
		</div>

		<form method="POST" id="promostion_update" action="<?= site_url("/admin/promostion/edit/".$sno ) ?>" enctype="multipart/form-data">
			<input type="hidden" class="txt_csrfname" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
			<input type="hidden" name="sno" value="<?=$sno;?>" />
			<?php if($sno != '0') { ?>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="eventName" name="eventName" placeholder="메거진 제목" value="<?=$promostion['eventName']; ?>">
					<label for="eventName" class="form-label">메거진 제목</label>
				</div>
				<div class="row g-2">
					<div class="col-md">
						<div class="form-floating mb-3">
							<input type="text" id="eventStart" name="eventStart" class="form-control" value="<?=$promostion['eventStartInfo']['date']; ?>" >
							<label for="eventStart" class="form-label">시작일</label>
						</div>
					</div>
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="eventStartHour" name="eventStartHour" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($hour as $val) { ?>
								<option value="<?=$val; ?>" <?php if($promostion['eventStartInfo']['hour'] == $val) { ?>selected="selected"<?php } ?> ><?=$val; ?></option>
							<?php } ?>
							</select>
							<label for="eventStartHour" class="form-label">시</label>
						</div>
					</div>
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="eventStartMinute" name="eventStartMinute" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($minute as $val) { ?>
								<option value="<?=$val; ?>" <?php if($promostion['eventStartInfo']['minute'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
							<?php } ?>
							</select>
							<label for="eventStartMinute" class="form-label">분</label>
						</div>
					</div>
				</div>
				
				<div class="row g-2">
					<div class="col-md">
						<div class="form-floating mb-3">
							<input type="text" id="eventEnd" name="eventEnd" class="form-control" value="<?=$promostion['eventEndInfo']['date']; ?>" >
							<label for="eventEnd" class="form-label">종료일</label>
						</div>
					</div>
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="eventEndHour" name="eventEndHour" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($hour as $val) { ?>
								<option value="<?=$val; ?>" <?php if($promostion['eventEndInfo']['hour'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
							<?php } ?>
							</select>
							<label for="eventEndHour" class="form-label">시</label>
						</div>
					</div>
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="eventEndMinute" name="eventEndMinute" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($minute as $val) { ?>
								<option value="<?=$val; ?>" <?php if($promostion['eventEndInfo']['minute'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
							<?php } ?>
							</select>
							<label for="eventEndMinute" class="form-label">분</label>
						</div>
					</div>
				</div>
				
				<?php if($promostion['eventThumbnail']) { ?>
					<div class="row g-2">
						<div class="col-md">
							<div class="form-floating mb-2">
								<input type="file" name="eventThumbnail" id="eventThumbnail" class="form-control" >
								<label for="eventThumbnail" class="form-label">PC 썸네일</label>
							</div>
						</div>
						<div class="col-md">
							<div class="form-floating mb-2">
								<input type="hidden" name="eventThumbnail" value="<?=$promostion['eventThumbnail'];?>" />
								<img src="<?= base_url('../uploads/' .$promostion['eventThumbnail'] )?>" style="width:150px;"/>
								<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
									<div class="btn-group me-2" role="group" aria-label="First group">
										<input type="checkbox" class="btn-check me-2" id="btncheck1" name="fileDel[eventThumbnail]" value="<?=$promostion['eventThumbnail']; ?>" autocomplete="off">
										<label class="btn btn-outline-primary" for="btncheck1">삭제</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php } else { ?>
					<div class="form-floating mb-3">
						<input type="file" name="eventThumbnail" id="eventThumbnail" class="form-control" >
						<label for="eventThumbnail" class="form-label">PC 썸네일</label>
					</div>
				<?php } ?>

				<?php if($promostion['eventThumbnailMobile']) { ?>
					<div class="row g-2">
						<div class="col-md">
							<div class="form-floating mb-2">
								<input type="file" name="eventThumbnailMobile" id="eventThumbnailMobile" class="form-control" >
								<label for="eventThumbnailMobile" class="form-label">MO 썸네일</label>
							</div>
						</div>
						<div class="col-md">
							<div class="form-floating mb-2">
								<input type="hidden" name="eventThumbnailMobile" value="<?=$promostion['eventThumbnailMobile'];?>" />
								<img src="<?= base_url('../uploads/' .$promostion['eventThumbnailMobile'] )?>" style="width:150px;"/>
								<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
									<div class="btn-group me-2" role="group" aria-label="First group">
										<input type="checkbox" class="btn-check me-2" id="btncheck2" name="fileDel[eventThumbnailMobile]" value="<?=$promostion['eventThumbnailMobile']; ?>" autocomplete="off">
										<label class="btn btn-outline-primary" for="btncheck2">삭제</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php } else { ?>
					<div class="form-floating mb-3">
						<input type="file" name="eventThumbnailMobile" id="eventThumbnailMobile" class="form-control" >
						<label for="eventThumbnailMobile" class="form-label">MO 썸네일</label>
					</div>
				<?php } ?>
				
				<?php if($promostion['eventImage']) { ?>
					<div class="row g-2">
						<div class="col-md">
						<div class="form-floating mb-2">
							<input type="file" name="eventImage" id="eventImage" class="form-control" >
							<label for="eventImage" class="form-label">PC 상세이미지</label>
						</div>
						</div>
						<div class="col-md">
							<div class="form-floating mb-2">
								<input type="hidden" name="eventImage" value="<?=$promostion['eventImage'];?>" />
								<img src="<?= base_url('../uploads/' .$promostion['eventImage'] )?>" style="width:150px;"/>
								<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
									<div class="btn-group me-2" role="group" aria-label="First group">
										<input type="checkbox" class="btn-check me-2" id="btncheck3" name="fileDel[eventImage]" value="<?=$promostion['eventImage']; ?>" autocomplete="off">
										<label class="btn btn-outline-primary" for="btncheck3">삭제</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php } else { ?>
					<div class="form-floating mb-3">
						<input type="file" name="eventImage" id="eventImage" class="form-control" >
						<label for="eventImage" class="form-label">PC 상세이미지</label>
					</div>
				<?php } ?>				
				
				
				<?php if($promostion['eventImageMobile']) { ?>
					<div class="row g-2">
						<div class="col-md">
						<div class="form-floating mb-2">
							<input type="file" name="eventImageMobile" id="eventImageMobile" class="form-control" >
							<label for="eventImageMobile" class="form-label">MO 상세이미지</label>
						</div>
						</div>
						<div class="col-md">
							<div class="form-floating mb-2">
								<input type="hidden" name="eventImageMobile" value="<?=$promostion['eventImageMobile'];?>" />
								<img src="<?= base_url('../uploads/' .$promostion['eventImageMobile'] )?>" style="width:150px;"/>
								<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
									<div class="btn-group me-2" role="group" aria-label="First group">
										<input type="checkbox" class="btn-check me-2" id="btncheck4" name="fileDel[eventImageMobile]" value="<?=$promostion['eventImageMobile']; ?>" autocomplete="off">
										<label class="btn btn-outline-primary" for="btncheck4">삭제</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php } else { ?>
					<div class="form-floating mb-3">
						<input type="file" name="eventImageMobile" id="eventImageMobile" class="form-control" >
						<label for="eventImageMobile" class="form-label">MO 상세이미지</label>
					</div>
				<?php } ?>	
				
				<div class="row g-2">
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="productCount" name="productCount" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($viewCnt as $val) { ?>
								<?php if($val > 1) { ?>
								<option value="<?=$val; ?>" <?php if($promostion['productCount'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
								<?php } ?>
							<?php } ?>
							</select>
							<label for="productCount" class="form-label">PC 행당 노출수</label>
						</div>
					</div>
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="productCountMobile" name="productCountMobile" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($viewCnt as $val) { ?>
								<?php if($val < 6 && $val != 0) { ?>
								<option value="<?=$val; ?>" <?php if($promostion['productCountMobile'] == $val) { ?>selected="selected"<?php } ?>><?=$val; ?></option>
							<?php } ?>
							<?php } ?>
							</select>
							<label for="productCountMobile" class="form-label">MO 행당 노출수</label>
						</div>
					</div>
				</div>

				<div class="row g-2">
					<div class="col-md">
						<div class="form-group mb-4">
							<label class="control-label col-sm-2" for="message">상세에디터</label>
							<div class="col-sm-10">
									<textarea class="form-control editor" id="message" name="message"><?= ($promostion['message']) ?></textarea>
							</div>
						</div>	
					</div>
				</div>

			<?php } else { ?>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" id="eventName" name="eventName" placeholder="메거진 제목" >
					<label for="eventName" class="form-label">메거진 제목</label>
				</div>
				<div class="row g-2">
					<div class="col-md">
						<div class="form-floating mb-3">
							<input type="text" id="eventStart" name="eventStart" class="form-control" >
							<label for="eventStart" class="form-label">시작일</label>
						</div>
					</div>
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="eventStartHour" name="eventStartHour" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($hour as $val) { ?>
								<option value="<?=$val; ?>"><?=$val; ?></option>
							<?php } ?>
							</select>
							<label for="eventStartHour" class="form-label">시</label>
						</div>
					</div>
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="eventStartMinute" name="eventStartMinute" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($minute as $val) { ?>
								<option value="<?=$val; ?>"><?=$val; ?></option>
							<?php } ?>
							</select>
							<label for="eventStartMinute" class="form-label">분</label>
						</div>
					</div>
				</div>
				
				<div class="row g-2">
					<div class="col-md">
						<div class="form-floating mb-3">
							<input type="text" id="eventEnd" name="eventEnd" class="form-control" >
							<label for="eventEnd" class="form-label">종료일</label>
						</div>
					</div>
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="eventEndHour" name="eventEndHour" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($hour as $val) { ?>
								<option value="<?=$val; ?>"><?=$val; ?></option>
							<?php } ?>
							</select>
							<label for="eventEndHour" class="form-label">시</label>
						</div>
					</div>
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="eventEndMinute" name="eventEndMinute" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($minute as $val) { ?>
								<option value="<?=$val; ?>"><?=$val; ?></option>
							<?php } ?>
							</select>
							<label for="eventEndMinute" class="form-label">분</label>
						</div>
					</div>
				</div>
				
				<div class="form-floating mb-3">
					<input type="file" name="eventThumbnail" id="eventThumbnail" class="form-control" >
					<label for="eventThumbnail" class="form-label">PC 썸네일</label>
				</div>

				<div class="form-floating mb-3">
					<input type="file" name="eventThumbnailMobile" id="eventThumbnailMobile" class="form-control" >
					<label for="eventThumbnailMobile" class="form-label">MO 썸네일</label>
				</div>
				
				<div class="form-floating mb-3">
					<input type="file" name="eventImage" id="eventImage" class="form-control" >
					<label for="eventImage" class="form-label">PC 상세이미지</label>
				</div>
				
				<div class="form-floating mb-3">
					<input type="file" name="eventImageMobile" id="eventImageMobile" class="form-control" >
					<label for="eventImageMobile" class="form-label">MO 상세이미지</label>
				</div>
				
				<div class="row g-2">
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="productCount" name="productCount" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($viewCnt as $val) { ?>
								<?php if($val > 1) { ?>
								<option value="<?=$val; ?>"><?=$val; ?></option>
								<?php } ?>
							<?php } ?>
							</select>
							<label for="productCount" class="form-label">PC 행당 노출수</label>
						</div>
					</div>
					<div class="col-md">
						<div class="form-floating mb-3">
							<select id="productCountMobile" name="productCountMobile" class="form-select" style="width:100%;" aria-label="Floating label select example">
							<?php foreach($viewCnt as $val) { ?>
								<?php if($val < 6 && $val != 0) { ?>
								<option value="<?=$val; ?>"><?=$val; ?></option>
							<?php } ?>
							<?php } ?>
							</select>
							<label for="productCountMobile" class="form-label">MO 행당 노출수</label>
						</div>
					</div>
				</div>
				
				<div class="row g-2">
					<div class="col-md">
						<div class="form-group mb-4">
							<label class="control-label col-sm-2" for="message">상세에디터</label>
							<div class="col-sm-10">
									<textarea class="form-control editor" id="message" name="message"></textarea>
							</div>
						</div>	
					</div>
				</div>
			<?php } ?>
			<button type="button" class="btn btn-success " target="_blank"  id="promostion_popup">상품등록</button>
			<div class="form-floating mb-3">
				<input type="hidden" name="product" id="dbk_product" class="form-control" value="<?=isset($promostion['product']) ? $promostion['product'] : ''; ?>" >
				<label for="product" class="form-label">상품선택</label>
			</div>
			<div>
				<div class="btn-group">
                	<button type="button" class="btn btn-white btn-icon-bottom js-moverow" data-direction="bottom">
						맨아래
					</button>
					<button type="button" class="btn btn-white btn-icon-down js-moverow" data-direction="down">
						아래
					</button>
					<button type="button" class="btn btn-white btn-icon-up js-moverow" data-direction="up">
						위
					</button>

					<button type="button" class="btn btn-white btn-icon-top js-moverow" data-direction="top">
						맨위
					</button>
				</div>
				<h1>선택된 상품</h1>
				<table class="table table-hover" id="codeListTbl" data-reorderable-rows="true" data-use-row-attr-func="false" data-toggle>
					<colgroup>
					<col style="width:94px;">
					<col style="width:280px;">
					<col style="width:400px;">
					<col style="width:280px;">
					<col style="width:400px;">
					<col style="width:300px;">
					<col style="width:300px;">
					</colgroup>
					<thead>
					<th>
						<input type="checkbox" class="form-checkbox" id="checkAddAll">
					</th>
					<th>이미지</th>
					<th>상품명</th>
					<th>공급사</th>
					<th>판매가</th>
					<th>재고</th>
					<th>품절여부</th>
					</thead>
					<tbody id="dbk_addProduct">
					<?php 
					if(isset($subProductList)) {
						foreach ($subProductList as $data) :
							if(isset($data['isSoldOut']) && $data['isSoldOut'] != '') {
								$soldOut = '품절';
							} else {
								$soldOut = '정상';
							}
						?>
						<tr id="tbl_add_goods_<?= $data['productNo'];?>" class="dbk_add_goods_free move-row" data-key="<?= $data['productNo'];?>">
							<td>
								<input type="hidden" name="productNo[]" value="<?=$data['productNo']?>" />
								<input type="checkbox" value="<?=$data['productNo']; ?>" class="form-checkbox dbk_add_checked" id="dbk_add_tr_<?= $data['productNo'];?>"/>
							</td>
							<td>
								<img src="<?=$data['listImageUrls'][0]?>" style="width:150px;"/>
							</td>
							<td>
								<span><?=$data['productName']; ?></span>
							</td>
							<td>
								<span><?=$data['partnerName']; ?></span>
							</td>
							<td>
								<?=number_format($data['salePrice']); ?>
							</td>
							<td>
								<?=number_format($data['stockCnt']); ?>
							</td>
							<td>
								<?=$soldOut; ?>
							</td>
						</tr>
					<?php endforeach; 
					} ?>
					</tbody>
				</table>
			</div>
		</form>
		<div class="d-grid gap-2 d-md-flex justify-content-md-end">
			<a href="/admin/promostion?<?=$queryParam; ?>">
				<button type="button" class="btn btn-primary">목록
				</button>
			</a>
			<?php if($sno != '0') { ?>
			<button type="button" class="btn btn-success " id="promostion_update_btn">수정
			</button>
			<form method="post" action="/admin/promostion/delete" >
				<input type="hidden" name="sno" value="<?=$promostion['sno']; ?>" />
				<button type="submit" class="btn btn-danger">삭제
				</button>
			</form>
			<?php } else { ?>
			<button type="button" class="btn btn-success " id="promostion_update_btn">저장
			</button>
			<?php } ?>
		</div>
	</div>
</div>
<script type="text/javascript">

    // Custom CKEditor file upload Adapter
    class MyUploadAdapter { 
		constructor( loader, sort ) { 
			this.loader = loader; 
			this.sort = sort
		} 
		upload() { 
			return this.loader.file 
				.then( file => new Promise( ( resolve, reject ) => { 
					this._initRequest(); 
					this._initListeners( resolve, reject, file ); 
					this._sendRequest( file ); 
			} ) ); 
		} 
		abort() { 
			if ( this.xhr ) { 
				this.xhr.abort(); 
			} 
		} 
		_initRequest() { 
			const xhr = this.xhr = new XMLHttpRequest(); 
			//POST 요청으로 파일을 업로드하는 페이지 지정 - json 으로 결과값 수신
			xhr.open( 'POST', '/admin/uploads', true ); 
			xhr.responseType = 'json'; 
		}

		//XHR 리스너 초기화 하기 
		_initListeners( resolve, reject, file ) { 
			const xhr = this.xhr; 
			const loader = this.loader; 
			const genericErrorText = "파일업로드 실패 (FILE:${ file.name })"; 
			xhr.addEventListener( 'error', () => reject( genericErrorText ) ); 
			xhr.addEventListener( 'abort', () => reject() ); 
			xhr.addEventListener( 'load', () => { 
				const response = xhr.response; 

				//에러처리
				if ( !response || response.error ) { 
					return reject( response && response.error ? response.error.message : genericErrorText ); 
				} 

				// 만약 업로드가 성공했다면, 업로드 프로미스를 적어도 default URL을 담은 객체와 함께 resolve하라. 
				// 이 URL은 서버에 업로드된 이미지를 가리키며, 컨텐츠에 이미지를 표시하기 위해 사용된다. 
				resolve( { default: response.url } );
			} ); 

				// 파일로더는 uploadTotal과 upload properties라는 속성 두개를 갖는다. 
				// 이 두개의 속성으로 에디터에서 업로드 진행상황을 표시 할 수 있다. 
				if ( xhr.upload ) { 
					xhr.upload.addEventListener( 'progress', evt => { 
					if ( evt.lengthComputable ) { 
						loader.uploadTotal = evt.total; 
						loader.uploaded = evt.loaded; 
					}
				}); 
				} 
			}

		//데이터 설정 및 서버전송
		_sendRequest( file ) { 
			const data = new FormData(); 
			data.append( 'upload', file ); 
			this.xhr.send( data ); 
		}

	}

    function CustomUploadAdapterPlugin( editor ) {
         editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {

              // CSRF hash
              var csrfName = document.getElementsByClassName('txt_csrfname')[0].name; 
              var csrfHash = document.getElementsByClassName('txt_csrfname')[0].value;

              // Upload URL
              var uploadURL = "<?=site_url('/admin/uploads')?>";

              return new MyUploadAdapter( loader, uploadURL,csrfName,csrfHash );
         };
    }
    // Initialize CKEditor on 'editor' class
    ClassicEditor
        .create( document.querySelector( '.editor' ),{ 
			toolbar: {
				items: ["heading", "|", "fontFamily", "fontSize", "fontColor", "fontBackgroundColor", "alignment", "|", "bold", "italic", "link", "bulletedList", "numberedList", "|", "outdent", "indent", "|", "imageUpload", "blockQuote", "insertTable", "mediaEmbed", "undo", "redo", "|", "code", "codeBlock", "htmlEmbed"]
			},
			language: 'ko',
			extraPlugins: [ CustomUploadAdapterPlugin ],
			uploadUrl: "<?=site_url('/admin/uploads')?>",
			mediaEmbed: {
				previewsInData:true
			},
			table: {
				contentToolbar: [
					'toggleTableCaption'
				]
			},
        } )
        .then( editor => {
              console.log( editor );
        } )
        .catch( error => {
              console.error( error );
        } );
</script>

<script>
    const element = document.getElementById("promostion_update_btn");
    element.addEventListener("click", (event) => {
        document.getElementById('promostion_update').submit();
    });

    document.addEventListener("DOMContentLoaded", () => {
        // 파일 다운로드
        /*
        document.getElementsByClassName("file_down").addEventListener("click", getFileDownload);
        
        $('.file_down').on('click', () => {
            const sno = $(this).data('sno');
            $.ajax({
                url: '/admin/promostion/down/'+sno
            }).done((data) => {
                alert('다운이 완료되었습니다.')
            });
        })
        */
    })
    function getFileDownload(sno) {
        //XMLHttpRequest 객체 생성
        var xhr = new XMLHttpRequest();

        //요청을 보낼 방식, 주소, 비동기여부 설정
        xhr.open('GET', "/admin/promostion/down/"+sno, true);
        xhr.responseType = "blob";
        xhr.setRequestHeader('my-custom-header', 'custom-value'); 

        //요청 전송
        xhr.send();

        //통신후 작업
        xhr.onload = () => {
            //통신 성공
            if (xhr.status == 200) {
                console.log(xhr.response);
                console.log("통신 성공");
            } else {
                //통신 실패
                console.log("통신 실패");
            }
        }
    }

	
    // $.datepicker.setDefaults($.datepicker.regional['ko']);
	$(function() {
		$('#eventStart').datepicker({
			assumeNearbyYear: true,
		    format: "yyyy-mm-dd",	//데이터 포맷 형식(yyyy : 년 mm : 월 dd : 일 )
		    startDate: '',// '-10d',	//달력에서 선택 할 수 있는 가장 빠른 날짜. 이전으로는 선택 불가능 ( d : 일 m : 달 y : 년 w : 주)
		    endDate: '', // '+10d',	//달력에서 선택 할 수 있는 가장 느린 날짜. 이후로 선택 불가 ( d : 일 m : 달 y : 년 w : 주)
		    autoclose : true,	//사용자가 날짜를 클릭하면 자동 캘린더가 닫히는 옵션
		    calendarWeeks : false, //캘린더 옆에 몇 주차인지 보여주는 옵션 기본값 false 보여주려면 true
		    clearBtn : false, //날짜 선택한 값 초기화 해주는 버튼 보여주는 옵션 기본값 false 보여주려면 true
		    datesDisabled : [],//선택 불가능한 일 설정 하는 배열 위에 있는 format 과 형식이 같아야함.
		    daysOfWeekDisabled : [],	//선택 불가능한 요일 설정 0 : 일요일 ~ 6 : 토요일
		    daysOfWeekHighlighted : [0,6], //강조 되어야 하는 요일 설정
		    disableTouchKeyboard : false,	//모바일에서 플러그인 작동 여부 기본값 false 가 작동 true가 작동 안함.
		    immediateUpdates: false,	//사용자가 보는 화면으로 바로바로 날짜를 변경할지 여부 기본값 :false 
		    multidate : false, //여러 날짜 선택할 수 있게 하는 옵션 기본값 :false 
		    multidateSeparator :",", //여러 날짜를 선택했을 때 사이에 나타나는 글짜 2019-05-01,2019-06-01
		    templates : {
		        leftArrow: '&laquo;',
		        rightArrow: '&raquo;'
		    }, //다음달 이전달로 넘어가는 화살표 모양 커스텀 마이징 
		    showWeekDays : true ,// 위에 요일 보여주는 옵션 기본값 : true
		    title: "시작일",	//캘린더 상단에 보여주는 타이틀
		    todayHighlight : true ,	//오늘 날짜에 하이라이팅 기능 기본값 :false 
		    toggleActive : true,	//이미 선택된 날짜 선택하면 기본값 : false인경우 그대로 유지 true인 경우 날짜 삭제
		    weekStart : 0 ,//달력 시작 요일 선택하는 것 기본값은 0인 일요일 
		    language : "ko"	//달력의 언어 선택, 그에 맞는 js로 교체해줘야한다.
		    
		});//datepicker end
		
		$('#eventEnd').datepicker({
		    format: "yyyy-mm-dd",	//데이터 포맷 형식(yyyy : 년 mm : 월 dd : 일 )
		    startDate: '',// '-10d',	//달력에서 선택 할 수 있는 가장 빠른 날짜. 이전으로는 선택 불가능 ( d : 일 m : 달 y : 년 w : 주)
		    endDate: '', // '+10d',	//달력에서 선택 할 수 있는 가장 느린 날짜. 이후로 선택 불가 ( d : 일 m : 달 y : 년 w : 주)
		    autoclose : true,	//사용자가 날짜를 클릭하면 자동 캘린더가 닫히는 옵션
		    calendarWeeks : false, //캘린더 옆에 몇 주차인지 보여주는 옵션 기본값 false 보여주려면 true
		    clearBtn : false, //날짜 선택한 값 초기화 해주는 버튼 보여주는 옵션 기본값 false 보여주려면 true
		    datesDisabled : [],//선택 불가능한 일 설정 하는 배열 위에 있는 format 과 형식이 같아야함.
		    daysOfWeekDisabled : [],	//선택 불가능한 요일 설정 0 : 일요일 ~ 6 : 토요일
		    daysOfWeekHighlighted : [0,6], //강조 되어야 하는 요일 설정
		    disableTouchKeyboard : false,	//모바일에서 플러그인 작동 여부 기본값 false 가 작동 true가 작동 안함.
		    immediateUpdates: false,	//사용자가 보는 화면으로 바로바로 날짜를 변경할지 여부 기본값 :false 
		    multidate : false, //여러 날짜 선택할 수 있게 하는 옵션 기본값 :false 
		    multidateSeparator :",", //여러 날짜를 선택했을 때 사이에 나타나는 글짜 2019-05-01,2019-06-01
		    templates : {
		        leftArrow: '&laquo;',
		        rightArrow: '&raquo;'
		    }, //다음달 이전달로 넘어가는 화살표 모양 커스텀 마이징 
		    showWeekDays : true ,// 위에 요일 보여주는 옵션 기본값 : true
		    title: "시작일",	//캘린더 상단에 보여주는 타이틀
		    todayHighlight : true ,	//오늘 날짜에 하이라이팅 기능 기본값 :false 
		    toggleActive : true,	//이미 선택된 날짜 선택하면 기본값 : false인경우 그대로 유지 true인 경우 날짜 삭제
		    weekStart : 0 ,//달력 시작 요일 선택하는 것 기본값은 0인 일요일 
		    language : "ko"	//달력의 언어 선택, 그에 맞는 js로 교체해줘야한다.
		    
		});//datepicker end

		$('#promostion_popup').on('click', function() {
			var popup = window.open('/admin/related_product?promostionStr=<?=isset($promostion['product']) ? $promostion['product'] : '' ?>','상품선택','width=700px,height=800px,scrollbars=yes,fullscreen=yes,target=_blank');
		});

		// 전체 클릭 
		$('#checkAddAll').on('click', function() {
			const checkedFlag = $(this).prop('checked');
			$('.dbk_add_checked').prop('checked', checkedFlag);
		});

		// 채크 박스 클릭
		$(document).on('click', '.dbk_add_goods_free', function(){
			$(this).find('.dbk_add_checked').prop("checked", !$(this).find('.dbk_add_checked').prop("checked"));
			// $(this).find('.dbk_add_checked').trigger('click');
			$('#checkAddAll').prop('checked', ($('.dbk_add_checked:checked').length == $('.dbk_add_checked').length));
		});

		// 상품 정렬 관련 
        // 위/아래이동 버튼 이벤트
        $('.js-moverow').click(function (e) {
            if (iciRow) {
                var idx = (iciRow.index('tr') + 1);
                switch ($(this).data('direction')) {
                    case 'up':
                        iciRow.moveRow(-1);
                        break;
                    case 'down':
                        iciRow.moveRow(1);
                        break;
                    case 'top':
                        iciRow.moveRow(-100);
                        break;
                    case 'bottom':
                        iciRow.moveRow(100);
                        break;
                }
            } else {
                alert('순서 변경을 원하시는 상품을 클릭해주세요.');
            }
        });


        // 리스트 클릭 활성/비활성화
        var iciRow = '';
        var preRow = '';
        $(document).on('click', '#codeListTbl tbody tr', function (e) {
            // 인풋박스 선택시 입력만 활성화
            if ($(e.target).is('span[class=color-selector-btn]') || $(e.target).is('input[type=text]') || $(e.target).is('button') || ($(e.target).is('td') && $(e.target).find('input[type=text]').length)) {
                return true;
            }

            // 이동할 셀 선택처리
            if (preRow && iciRow) {
                $(this).siblings().each(function () {
                    $(this).removeClass('warning');
                });
                preRow = iciRow = '';
            } else {
                if (iciRow) preRow = iciRow;
                iciRow = $(this);
                iciRow.addClass('warning');
                if (preRow && iciRow) {
                    var preRowIndex = (preRow.index('.move-row') + 1);
                    var iciRowIndex = (iciRow.index('.move-row') + 1);

                    if (preRowIndex == iciRowIndex) {
                        $(this).removeClass('warning');
                        preRow = iciRow = '';
                    } else {
                        if (preRowIndex > iciRowIndex) {
                            var startNo = iciRowIndex;
                            var endNo = preRowIndex;
                        } else {
                            var startNo = preRowIndex;
                            var endNo = iciRowIndex;
                        }
                        for (var j = startNo; j <= endNo; j++) {
                            $(this).siblings().each(function () {
                                var thisFormRow = $(this).index('.move-row') + 1;
                                if (thisFormRow == j) {
                                    $(this).addClass('warning');
                                }
                            });
                        }
                    }
                }
            }
        });

	});//ready end

	// 팝업에서 받은 내용 넣는 과정
	function get_product_div(innerHtmlData, productVal) {
		/*
		$('#product').val(product);
		$('#addProduct').remove().html(data);
		*/
		// console.log(data)
		console.log(productVal)
		// alert('파일등록 저장을 완료해주세요.')
	}

/**
 * 리스트 이동
 *
 * @param integer step 이동 갯수 (1, -1)
 */
jQuery.fn.moveRow = function (step) {
    var thisTable = this.parent();
    var tableHeight = thisTable.parent().height();
    var selectZone = thisTable.find('>tr');
    var selectIndex = thisTable.data('index');
    if(selectIndex== undefined) selectIndex = ".move-row";
    var startIndex = 0;
    var endIndex = 0;
    var idx = 0;
    if (step < 0) {
        var useBefore = true;
    }

    $(selectZone).each(function () {
        var obj = $(this);
        if (obj.hasClass('warning')) {
            if (startIndex == 0) {
                startIndex = (obj.index(selectIndex) + 1);
            }
            endIndex = (obj.index(selectIndex) + 1);
        }
    });

    // 위로 이동시
    if (step < 0) {
        if (startIndex != 1) {
            var targetZone = (step < -1 ? 0 : startIndex - 2);
            for (var i = startIndex; i <= endIndex; i++) {
                idx = i - 1;
                if(selectZone.eq(idx).hasClass('warning')) {
                    if(step=='-100') targetZone = 0;
                    selectZone.eq(idx)['insert' + (useBefore && 'Before' || 'After')](selectZone.eq(targetZone));
                } else {
                    targetZone = idx;
                }
            }
        }
        // 아래로 이동시
    } else {
        var targetZone = (step > 1 ? selectZone.length - 1 : endIndex);
        for (var i = endIndex; i >= startIndex; i--) {
            idx = i - 1;
            if(selectZone.eq(idx).hasClass('warning')) {
                if(step=='100') targetZone = selectZone.length-1;
                selectZone.eq(idx)['insert' + (useBefore && 'Before' || 'After')](selectZone.eq(targetZone));
            } else {
                targetZone = idx;
            }
        }
    }

    thisTable.parent().height(tableHeight);
	// $('#dbk_product')
	
	var productNos = new Array();
	$('.dbk_add_checked').each(function() {
		productNos.push($(this).val());
	});
	
	$('#dbk_product').val(productNos.join('^|^'));
}

</script>