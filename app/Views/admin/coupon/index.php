<div id="coupon" class="coupon-view">
	<div class="inner">
		<div class="titleArea">
			<h2>특정 쿠폰 설정</h2>
		</div>

		<form method="POST" id="coupon_update" action="/admin/coupon/edit" >
			<input type="hidden" name="sno" value="<?=$coupon['sno']; ?>" />
			<div class="form-floating mb-3">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="useFl" id="useFlY" value="y" <?php if($coupon['useFl'] == 'y') { ?>checked <?php } ?>>
					<label class="form-check-label" for="useFlY">
						사용함
					</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="useFl" id="useFlN" value="n" <?php if($coupon['useFl'] == 'n') { ?>checked <?php } ?>>
					<label class="form-check-label" for="useFlN">
						사용안함
					</label>
				</div>
			</div>
			<div class="form-floating mb-3">
				<input type="text" class="form-control" id="productNo" name="productNo" value="<?php if($coupon['productNo'] != '0') { ?><?=$coupon['productNo']; ?> <?php } ?>"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="상품번호">
				<label for="productNo" class="form-label">상품번호</label>
			</div>
			<div class="form-floating mb-3">
				<input type="text" class="form-control" id="couponNo" name="couponNo" value="<?php if($coupon['couponNo'] != '0') { ?><?=$coupon['couponNo']; ?> <?php } ?>"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"  placeholder="쿠폰번호">
				<label for="couponNo" class="form-label">쿠폰번호</label>
				<?php if(isset($errors['couponNo'])) { ?>
				<div class="valid-feedback">
					<?=$errors['couponNo']; ?>
				</div>
				<?php } ?>
			</div>
		</form>
		<div class="d-grid gap-2 d-md-flex justify-content-md-end">
			<button type="button" class="btn btn-success " id="coupon_update_btn">수정
			</button>
		</div>
	</div>
</div>

<script>
    const element = document.getElementById("coupon_update_btn");
    element.addEventListener("click", (event) => {
        document.getElementById('coupon_update').submit();
    });
</script>