<?php
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class CheckAdminRole implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        // 세션에서 사용자 역할을 확인하고, 권한이 없는 경우 리다이렉트
        $session = session();
        $userRole = $session->get('user_role');

        //임시로 닫음
        /*
        if ($userRole !== 'admin') {
            return redirect()->to('/front/propose');
        }
        */
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // 필터 후처리
    }
}