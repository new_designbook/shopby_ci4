<?php

if ( ! function_exists('admin_render'))
{
    function admin_render(string $name, array $data = [], array $options = [])
    {
        return view(
            'admin/layout/layout',
            [
                'content' => view($name, $data, $options),
            ],
            $options,
            
        );
    }
}

if ( ! function_exists('front_render'))
{
    function front_render(string $name, array $data = [], array $options = [])
    {
        return view(
            'front/layout/layout',
            [
                'content' => view($name, $data, $options),
            ],
            $options,
            
        );
    }
}

if ( ! function_exists('mobile_render'))
{
    function mobile_render(string $name, array $data = [], array $options = [])
    {
        return view(
            'mobile/layout/layout',
            [
                'content' => view($name, $data, $options),
            ],
            $options,
            
        );
    }
}
