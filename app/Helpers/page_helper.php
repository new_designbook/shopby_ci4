<?php
if (!function_exists('idx')) {
    function idx($currentPage, $perPage, $totalPage)
    {
        if (is_null($totalPage)) {
            $idx = $totalPage;
        } else {
            $idx = $totalPage - ($perPage * ($currentPage - 1));
        }
        return $idx;
    }
}

