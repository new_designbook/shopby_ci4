<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'propose';
    protected $primaryKey = 'sno';

    // ...

    public function getPagination(?int $perPage = 20): array
    {
        $totalCnt = $this->builder()
            ->select('propose.*')
            //->join('category', 'news.category_id = category.id');
            ->countAll();
        return [
            'data'  => $this->paginate($perPage,'num'),
            'pager' => $this->pager,
            'totalCnt' => $totalCnt,
        ];
    }
}