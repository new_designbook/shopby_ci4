<?php 

namespace App\Models;

use CodeIgniter\Model;

class FileModel extends Model
{
    protected $table      = 'file';
	protected $primaryKey = 'sno';

    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at'];
    protected $casts   = [];

    protected $allowedFields = ['tableName', 'fieldName', 'originalName', 'name', 'originalMimeType', 'size', 'pathName' ];
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
    protected $useSoftDeletes = false; // deleted_at 사용안함

	protected $useAutoIncrement = true;

    protected $returnType = "array";

}