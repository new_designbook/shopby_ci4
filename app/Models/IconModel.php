<?php 

namespace App\Models;

use CodeIgniter\Model;

class IconModel extends Model
{
    protected $table      = 'dbk_icon';
	protected $primaryKey = 'sno';

    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];

    protected $allowedFields = ['icon1', 'icon2', 'icon3','icon4', 'icon5', 'icon6','icon7', 'icon8', 'icon9','icon10', 'icon11', 'icon12','icon13', 'icon14', 'icon15','icon16', 'icon17', 'icon18', 'icon19', 'icon20'];
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    protected $useSoftDeletes = true;
    
	protected $useAutoIncrement = true;

    protected $returnType = "App\Entities\IconEntities";

}