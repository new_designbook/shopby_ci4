<?php 

namespace App\Models;

use CodeIgniter\Model;

class CouponModel extends Model
{
    protected $table      = 'dbk_coupon';
	protected $primaryKey = 'sno';

    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];

    protected $allowedFields = ['useFl', 'productNo', 'couponNo'];
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    protected $useSoftDeletes = true;

	protected $useAutoIncrement = true;

    protected $returnType = "App\Entities\CouponEntities";

    protected $validationRules = [
		'couponNo' => 'required',
    ];

    protected $validationMessages = [
        'couponNo' => [
            'required' => '쿠폰번호를 입력하세요'
        ],
    ];
}