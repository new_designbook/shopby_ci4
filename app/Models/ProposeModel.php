<?php 

namespace App\Models;

use CodeIgniter\Model;

class ProposeModel extends Model
{
    protected $table      = 'propose';
	protected $primaryKey = 'sno';

    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];

    protected $allowedFields = ['kind', 'company', 'person', 'position', 'email', 'phone', 'homepage', 'company_letter', 'product_letter', 'export_history', 'sku', 'etc', 'file'];
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    protected $useSoftDeletes = true;

	protected $useAutoIncrement = true;

    protected $returnType = "App\Entities\ProposeEntities";

    protected $validationRules = [
		'company' => 'required',
		'person' => 'required',
		'email' => 'required',
		'phone' => 'required',
    ];

    protected $validationMessages = [
        'company' => [
            'required' => '회사명을 입력하세요'
        ],
        'person' => [
            'required' => '담당자명을 입력하세요'
        ],
        'email' => [
            'required' => '이메일을 입력하세요',
        ],
        'phone' => [
            'required' => '전화번호를 입력하세요'
        ],
    ];

    /*
    protected function create($post_data)
    {
        $postEntity = new PostEntity();
        $postEntity->fill($post_data);
        $postEntity->author = $memberId;

        $postModel = new PostsModel();
        $post_id = $postModel->insert($postEntity);

        if ($post_id) {
            return [true, $post_id, []];
        }

        return [false, null, $postModel->errors()];
    }
    */

	// protected $beforeInsert = ['generateSlug'];
    /*
	protected function generateSlug(array $data)
	{
		$slug = strtolower(url_title($data['data']['name']));
		$name = trim($data['data']['name']);

		$category = $this->where('name', $name)->orderBy('id', 'DESC')->first();
		if ($category) {
			$slugs = explode('-', $category->slug);
			$slugNumber = !(empty($slugs[1])) ? ((int)$slugs[1] + 1) : 1;
			$slug = $slug. '-' .$slugNumber;
		}

		$data['data']['slug'] = $slug;

		return $data;
	}

	public function getParentOptions($exceptCategoryId = null)
	{
		$categories = [];

		$categoryModel = $this;
		if ($exceptCategoryId) {
			$categoryModel->where('id !=', $exceptCategoryId);
		}

        if ($results = $categoryModel->findAll()) {
            foreach ($results as $result) {
                $categories[] = [
                    'id' => $result->id,
                    'name' => $result->name,
                    'slug' => $result->slug,
                    'parent_id' => $result->parent_id,
                    'created_at' => $result->created_at,
                    'updated_at' => $result->updated_at,
                ];
            }
		}

		return $categories;
	}

	public function getNestedCategories($level = 0)
	{
		$results = [];
		$categories = $this->where('parent_id', $level)
			->orderBy('name', 'asc')
			->findAll();

		if (count($categories) > 0) {
			foreach ($categories as $key => $category) {
				$results[$key] = $category->toArray();
				$results[$key]['children'] = $this->getNestedCategories($category->id);
			}
		}

		return $results;
	}    

    */
}