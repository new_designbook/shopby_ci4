<?php

namespace App\Models;

use CodeIgniter\Model;

class Cafe24Token extends Model
{
    protected $db;

    function __construct() {
        parent::__construct();
        $this->db = db_connect();
    }

    //토큰 테이블 제일 마지막 데이터 추출
    public function GetClientTokenInfo($mallId)
    {
        return $this->db
					->table('client_token')
					//->where(["mall_id" => $mallId, "client_id" => "abcdefg"])
                    ->where(["mall_id" => $mallId])
					->get()
                    ->getLastRow();
    }

    //토큰 발급 프로세스
    public function cafe24TokenIssue($type, $code = null, $mallId, $refresh_token = null) 
    {
        $sThisUrl = 'https://egongegong.kr/redirect';  // https:// 사용

        if ($type === 'r') 
        {
            $fields = array(
                'grant_type'   => 'refresh_token',
                'refresh_token'=> $refresh_token,
                'redirect_uri' => $sThisUrl
            );
        }else 
        {
            $fields = array(
                'grant_type'   => 'authorization_code',
                'code'         => $code,
                'redirect_uri' => $sThisUrl
            );
        }

        $oCurl = curl_init();
        curl_setopt_array($oCurl, array(
            CURLOPT_URL            => 'https://' . $mallId . '.cafe24api.com/api/v2/oauth/token',
            CURLOPT_POSTFIELDS     => http_build_query($fields),
            CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
            CURLOPT_HTTPHEADER     => array(
                'authorization: Basic ' . base64_encode(env('cafe24.clientId') . ':' . env('cafe24.clientSecretKey')),
                'content-type: application/x-www-form-urlencoded'
            )
        ));
        $sResponse = curl_exec($oCurl);
        // 정상 토큰발급 응답인지 확인용 출력해보기
        $resAry = json_decode($sResponse, true);
         /*
        if($resAry) {
        }else {
            //테스트용
           
            $resAry = [
                'mall_id' => 'wwwwww',
                'client_id' => '22222'
            ];
        }
        */
		// echo print_r($resAry,true).'<br/>';
		$resAryData['access_token'] = $resAry['access_token'];
		$resAryData['expires_at'] = $resAry['expires_at'];
		$resAryData['refresh_token'] = $resAry['refresh_token'];
		$resAryData['refresh_token_expires_at'] = $resAry['refresh_token_expires_at'];
		$resAryData['mall_id'] = $resAry['mall_id'];
		$resAryData['client_id'] = $resAry['client_id'];
		$resAryData['user_id'] = $resAry['user_id'];
		$resAryData['token_step'] = 'n';

		if($resAry['access_token']) {
			$this->SetClientTokenInfo($resAryData, 'n');
		}

        return $resAry['access_token'];
    }

    //발급 받은 토큰값 저장
    public function SetClientTokenInfo($data, $step) {
        return $this->db
                    ->table('client_token')
                    ->insert($data);
    }
}