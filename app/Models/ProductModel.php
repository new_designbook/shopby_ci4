<?php 

namespace App\Models;

use CodeIgniter\Model;

class ProductModel extends Model
{
    protected $table      = 'dbk_product';
	protected $primaryKey = 'product_no';

    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    
    protected $allowedFields = ['product_no','list_movie_link','detail_movie_link_1','detail_movie_link_2'];
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    protected $useSoftDeletes = true;

	protected $useAutoIncrement = true;
    //protected $returnType = "array";
    protected $returnType = 'App\Entities\ProductEntities';
    protected $validationRules = [];
    protected $validationMessages = [];
}