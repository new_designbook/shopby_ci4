<?php 

namespace App\Models;

use CodeIgniter\Model;

class PromostionModel extends Model
{
    protected $table      = 'dbk_promostion';
	protected $primaryKey = 'sno';

    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];

    protected $allowedFields = ['eventName', 'eventStart', 'eventEnd', 'eventThumbnail', 'eventThumbnailMobile', 'eventImage', 'eventImageMobile', 'productCount', 'productCountMobile', 'product', 'message'];
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    protected $useSoftDeletes = true;

	protected $useAutoIncrement = true;

    protected $returnType = "App\Entities\PromostionEntities";

    protected $validationRules = [
		'eventName' => 'required',
		'eventStart' => 'required',
		'eventEnd' => 'required',
    ];

    protected $validationMessages = [
        'eventName' => [
            'required' => '메거진 제목을 입력해주세요.'
        ],
        'eventStart' => [
            'required' => '시작일을 입력해주세요.'
        ],
        'eventEnd' => [
            'required' => '종료일을 입력해주세요.'
        ],
    ];
}