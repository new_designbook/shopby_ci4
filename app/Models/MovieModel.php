<?php 

namespace App\Models;

use CodeIgniter\Model;

class MovieModel extends Model
{
    protected $table      = 'dbk_movie';
	protected $primaryKey = 'sno';

    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    
    protected $allowedFields = ['pc_movie_url_1','pc_movie_url_2','mo_movie_url_1','mo_movie_url_2'];
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    protected $useSoftDeletes = true;

	protected $useAutoIncrement = true;
    //protected $returnType = "array";
    protected $returnType = 'App\Entities\MovieEntities';
    protected $validationRules = [];
    protected $validationMessages = [];
}