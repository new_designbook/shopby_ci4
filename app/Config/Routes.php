<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
//$routes->get('/dbk/oauth/redirect', 'Dbk\Oauth\Redirect::index');
$routes->get('/redirect', 'Redirect::index');
//, ['filter' => 'checkAdminRole']  두번째 인자에 해당 필터 적용 해야 URL로 접근 불가능
$routes->group('admin', ['filter' => 'checkAdminRole'], static function ($routes) {
	$routes->get('/', 'Admin\Index::index');
	$routes->get('temp', 'Admin\Temp::index');
	//변경 하여 재사용 예정
	$routes->get('propose', 'Admin\Propose::index');
	$routes->get('propose/show/(:num)', 'Admin\Propose::show/$1');
	$routes->post('propose/delete', 'Admin\Propose::delete');
	$routes->post('propose/edit/(:num)', 'Admin\Propose::edit/$1');
	$routes->get('propose/down/(:num)', 'Admin\Propose::getFileDownload/$1');

	// 프론트 스크립트
	$routes->get('script', 'Admin\Script::index');
	$routes->post('script', 'Admin\Script::create');
	$routes->put('script/(:num)', 'Admin\Script::scriptDelete/$1');
	//변경 하여 재사용 예정

	// 동영상 관리
	$routes->get('movie', 'Admin\MovieContorller::show/1');
	$routes->post('movie/edit', 'Admin\MovieContorller::edit');
	
	// 특정 쿠폰 관리
	$routes->get('coupon', 'Admin\CouponController::show/1');
	$routes->post('coupon/edit', 'Admin\CouponController::edit');

	// 번들상품 아이콘 관리
	$routes->get('icon', 'Admin\IconController::show/1');
	$routes->post('icon/edit', 'Admin\IconController::edit');

	//상품 리스트
	$routes->get('product', 'Admin\ProductController::index');
	$routes->get('product/show/(:num)', 'Admin\ProductController::show/$1');
	$routes->post('product/edit', 'Admin\ProductController::edit');

	// 번들상품 아이콘 관리
	$routes->get('promostion', 'Admin\PromostionController::index');
	$routes->get('promostion/show/(:num)', 'Admin\PromostionController::show/$1');
	$routes->post('promostion/delete', 'Admin\PromostionController::delete');
	$routes->post('promostion/edit/(:num)', 'Admin\PromostionController::edit/$1');
	$routes->get('promostion/down/(:num)', 'Admin\PromostionController::getFileDownload/$1');
	
	// 관련 상품 팝업 관련
	$routes->get('related_product', 'Admin\RelatedProductController::index');

	// 에디터 관련 파일 업로드 sjh
	$routes->post('uploads', 'Admin\FileController::index');
	$routes->get('uploads/show/(:string)', 'Admin\FileController::show');
});

$routes->group('front', static function ($routes) {
	$routes->get('propose', 'Front\Propose::index');
	$routes->post('propose', 'Front\Propose::create');
	$routes->get('articles', 'Front\Articles::index');
    
    // $routes->get('propose', 'Home::index');
	// 쿠폰 번호 / 상품 번호 조회하기
	$routes->get('coupon_info', 'Front\CouponApiController::index');

	//메인 동영상 주소 반환
	$routes->get('movie_api_detail', 'Front\MovieApiController::index');
	
	// 상품 아이콘 노출
	$routes->get('icon_info', 'Front\IconApiController::index');

	// 프로모션
	$routes->get('promostion_list', 'Front\PromosionApiController::index');
	$routes->get('promostion_detail/(:num)', 'Front\PromosionApiController::show/$1');

	//리스트 동영상 주소 반환
	$routes->get('product_movie_api_detail', 'Front\ProductApiController::list');
	$routes->get('product_movie_api_detail/show/(:num)', 'Front\ProductApiController::show/$1');
});


$routes->group('mobile', static function ($routes) {
	$routes->get('propose', 'Mobile\Propose::index');
	$routes->post('propose', 'Mobile\Propose::create');
    
    // $routes->get('propose', 'Home::index');
});
