<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class MovieEntities extends Entity
{
    private function to_markdown($content)
    {
        $content = str_replace(PHP_EOL, "  " . PHP_EOL, $content);
        return Markdown::defaultTransform($content);
    }

    /**
	 * @var array $casts
	 */
	protected $casts = [
		'sno'            => '?int',
		'pc_movie_url_1'      => '?string',
		'pc_movie_url_2'      => '?string',
		'mo_movie_url_1'      => '?string',
		'mo_movie_url_2'      => '?string',
		'created_at'    => '?datetime',
		'updated_at'    => '?datetime',
		'deleted_at'    => '?datetime'
	];
}
