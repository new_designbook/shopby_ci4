<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class CouponEntities extends Entity
{
    private function to_markdown($content)
    {
        $content = str_replace(PHP_EOL, "  " . PHP_EOL, $content);
        return Markdown::defaultTransform($content);
    }
}
