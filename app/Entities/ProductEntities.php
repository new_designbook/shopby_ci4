<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class ProductEntities extends Entity
{
    private function to_markdown($content)
    {
        $content = str_replace(PHP_EOL, "  " . PHP_EOL, $content);
        return Markdown::defaultTransform($content);
    }

    /**
	 * @var array $casts
	 */
	protected $casts = [
		'sno'            => '?int',
		'product_no'      => '?string',
		'list_movie_link'      => '?string',
		'detail_movie_link_1'      => '?string',
		'detail_movie_link_2'      => '?string',
		'created_at'    => '?datetime',
		'updated_at'    => '?datetime',
		'deleted_at'    => '?datetime'
	];
}
