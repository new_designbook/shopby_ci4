<?php

namespace App\Services;

use App\Entities\CouponEntities;
use App\Models\CouponModel;

class CouponService
{
    private static $couponService = null;

    public static function factory()
    {
        if (self::$couponService === null) {
            self::$couponService = new CouponService();
        }

        return self::$couponService;
    }

    public function frontFind($sno){
        $couponModel = new CouponModel();
        return $couponModel->where('useFl', 'y')->find($sno);
    }

    public function find($sno){
        
        $couponModel = new CouponModel();
        return $couponModel->find($sno);
    }

    public function update($coupon, $new_post_data)
    {
        $coupon->fill($new_post_data);
        $couponModel = new CouponModel();
        $isSuccess = $couponModel->save($coupon);
        return [$isSuccess, $couponModel->errors()];
    }

    public function delete($sno)
    {        
        $coupon = $this->find($sno);

        if (!$coupon) {
            return false;
        }

        $couponModel = new CouponModel();
        $couponModel->delete($coupon->sno);

        return true;
    }

}