<?php

namespace App\Services;

use App\Models\PromostionModel;

class PromostionService
{
    private static $promostionService = null;

    public static function factory()
    {
        if (self::$promostionService === null) {
            self::$promostionService = new PromostionService();
        }

        return self::$promostionService;
    }

    public function find($sno){
        $mPromostionModel = new PromostionModel();
        return $mPromostionModel->find($sno);
    }

    public function update($icon, $new_post_data)
    {
        $icon->fill($new_post_data);
        $mPromostionModel = new PromostionModel();
        $isSuccess = $mPromostionModel->save($icon);
        return [$isSuccess, $mPromostionModel->errors()];
    }

    public function delete($sno)
    {        
        $icon = $this->find($sno);

        if (!$icon) {
            return false;
        }

        $mPromostionModel = new PromostionModel();
        $mPromostionModel->delete($icon->sno);

        return true;
    }

}