<?php
namespace App\Services;

use App\Models\Cafe24Token;

class ScriptService
{
    protected $session;
    protected $tokenDb;

    public function __construct()
    {
        $this->session = \Config\Services::session();
        $this->tokenDb = new Cafe24Token();
    }

    private static $scriptService = null;

    public static function factory()
    {
        if (self::$scriptService === null) {
            self::$scriptService = new ScriptService();
        }

        return self::$scriptService;
    }

	public function getScript()
	{
		$param = [];
		/**
		 *	param = {
		 *	 srcipt_no : 스크립트 번호,
		 *	 src : 원본 script 경로,
		 *	 display_location : 화면 경로,
		 *	 exclude_path : 제외 경로,
		 *	 integrity : 하위 리소스 무결성
		 */
	
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => sprintf('https://%s.cafe24api.com/api/v2/admin/scripttags', 'egongegong2020').'?'.http_build_query($param, '', ),
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->getToken('egongegong2020'),
			'Content-Type: application/json'
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		if ($err) {
			echo 'cURL Error #:' . $err;
		} else {
			return json_decode($response, true);
		}
	}



	public function create()
	{
		header("Access-Control-Allow-Origin:*");
		$param = [];
		/**
		 *	param = {
		 *	 src : 원본 script 경로,
		 *	 display_location : 화면 경로,
		 *	 exclude_path : 제외 경로,
		 *	 integrity : 하위 리소스 무결성
			https://velog.io/@gsuchoi/Linux-CORS-%EC%84%A4%EC%A0%95-%EB%B0%A9%EB%B2%95
			블로그 참고하여 httpd.conf 에
			<IfModule mod_headers.c>
			   Header set Access-Control-Allow-Origin "*"
			</IfModule>
			추가함
		 */
		 // 
		$param['request']['src'] = 'https://egongegong.kr/js/dbk_front_article.js';
		$param['request']['display_location'] = [
			'BOARD_GALLERY_LIST'
		];
		// 무결성 해시 
		$param['request']['integrity'] = "sha384-".$this->getItegrity();

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => sprintf('https://%s.cafe24api.com/api/v2/admin/scripttags', 'egongegong2020'),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => json_encode($param, true),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->getToken('egongegong2020'),
				'Content-Type: application/json',
				'Access-Control-Allow-Origin: *'
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		if ($err) {
			echo 'cURL Error #:' . $err;
		} else {
			return json_decode($response, true);
		}
	}

	public function scriptDelete($script_no)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => sprintf('https://%s.cafe24api.com/api/v2/admin/scripttags/%s', 'egongegong2020', $script_no),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => 'DELETE',
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->getToken('egongegong2020'),
				'Content-Type: application/json'
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		if ($err) {
			echo 'cURL Error #:' . $err;
		} else {
			echo $response;
		}
	}

    protected function getToken($mallId)
    {
        //토큰 만료 체크를 위한 시간설정 (현재 시간보다 2시간뒤 시간체크 - 작업을 체크하기 위함)
        $timenow = date("Y-m-d H:i:s");
        $twoTimenow = date("Y-m-d His", strtotime($timenow."+30 minutes"));
        
        //몰아이디
        $this->session->set('mall_id', $mallId);

        //토큰 확인 후 없으면 신규발급n, 있으면 사용u, 만료면e 재발급 분기로 구분
		$token = $this->tokenDb->GetClientTokenInfo($mallId);//배열로 받아오기
        //토큰이 사용 가능할 시

        if($token)
        {
            if($token->access_token && strtotime($token->expires_at) >= strtotime($twoTimenow)) {
				$this->session->set('app_token', $token->access_token);

				return $token->access_token;
            } else {
				return $this->tokenDb->cafe24TokenIssue('r', null, $mallId, $token->refresh_token);        
            }
        } 
    }

	
	protected function getItegrity(){
		// $sFileSrc = public_path('/js/dbk_front_article.js');
		$sFileSrc = '../public/js/dbk_front_article.js';
        $oFileData = file_get_contents($sFileSrc);
		$sEncodedData = base64_encode(hash('sha384', $oFileData, true));
		return $sEncodedData;
	}

}