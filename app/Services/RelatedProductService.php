<?php

namespace App\Services;

class RelatedProductService
{
    private static $relatedProductService = null;

    public static function factory()
    {
        if (self::$relatedProductService === null) {
            self::$relatedProductService = new RelatedProductService();
        }
        return self::$relatedProductService;
    }

    /**
     *  [관리자] 리스트
     */
    public function product_list($param)
    {
        $clientid = $_ENV['app.ClientId'];
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  // CURLOPT_URL => 'https://server-api.e-ncp.com/products/search'.'?'.http_build_query($param),
          CURLOPT_URL => 'https://shop-api.e-ncp.com/products/search'.'?'.http_build_query($param),
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$_ENV['app.Token'],
			// 'Content-Type: application/json;charset=UTF-8',
            'accept: application/json;charset=UTF-8',
            'clientid: '.$clientid,
            'platform: PC',
            'version: 1.0'
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		if ($err) {
			echo 'cURL Error #:' . $err;
		} else {

			$productList = json_decode($response, true);
            return $productList;
        }
    }

    /**
     *  [관리자] 리스트
     */
    public function product_add_list($param)
    {
        $clientid = $_ENV['app.ClientId'];
		$curl = curl_init();
		curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://shop-api.e-ncp.com/products/search-by-nos',
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSLVERSION => true,
		  CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => json_encode($param, true),
		  CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$_ENV['app.Token'],
			'Content-Type: application/json;charset=UTF-8',
            'accept: application/json;charset=UTF-8',
            'clientid: '.$clientid,
            'platform: PC',
            'version: 1.0'
		  )
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		if ($err) {
			echo 'cURL Error #:' . $err;
		} else {

			$productList = json_decode($response, true);
            $products = $productList['products'];
            $result = [];
            foreach($products as $key => $product) {
                $result[$key]['productNo'] = $product['baseInfo']['productNo'];
                $result[$key]['productName'] = $product['baseInfo']['productName'];
                $result[$key]['partnerName'] = $product['baseInfo']['partnerName'];
                $result[$key]['stockCnt'] = $product['baseInfo']['stockCnt'];
                $result[$key]['listImageUrls'] = $product['baseInfo']['listImageUrls'];
                $result[$key]['salePrice'] = $product['price']['salePrice'];
                $result[$key]['isSoldOut'] = $product['status']['soldout'];
            }
            return $result;
        }
    }
}