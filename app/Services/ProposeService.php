<?php

namespace App\Services;

use App\Entities\ProposeEntities;
use App\Models\ProposeModel;

class ProposeService
{
    private static $proposeService = null;

    public static function factory()
    {
        if (self::$proposeService === null) {
            self::$proposeService = new ProposeService();
        }

        return self::$proposeService;
    }

    public function create($propose_data)
    {
        $proposeEntities = new ProposeEntities();
        $proposeEntities->fill($propose_data);
        $proposeModel = new ProposeModel();
        $sno = $proposeModel->insert($proposeEntities);

        if ($sno) {
            return [true, $sno, []];
        }

        return [false, null, $proposeModel->errors()];
    }

    public function find($sno){
        $proposeModel = new ProposeModel();
        return $proposeModel->find($sno);
    }

    public function isAuthor($propose, $sno){
        return $propose->isAuthor($sno);
    }

    public function update($propose, $new_post_data)
    {
        $propose->fill($new_post_data);
        $proposeModel = new ProposeModel();
        $isSuccess = $proposeModel->save($propose);
        return [$isSuccess, $proposeModel->errors()];
    }

    public function delete($sno)
    {        
        $propose = $this->find($sno);

        if (!$propose) {
            return false;
        }

        $proposeModel = new ProposeModel();
        $proposeModel->delete($propose->sno);

        return true;
    }

    /**
     *  [관리자] 리스트
     */
    public function propose_list($page, $kind, $perPage, $orderby)
    {
        $model = new ProposeModel();
        $propose_query =  $model->orderBy("created_at", $orderby);

        $propose_list = $model->where('kind', $kind)->paginate($perPage, 'propose', $page);

        $pager = $model->pager;
        
        return [$pager, $propose_list];
    }

}