<?php

namespace App\Services;

use App\Entities\IconEntities;
use App\Models\IconModel;

class IconService
{
    private static $iconService = null;

    public static function factory()
    {
        if (self::$iconService === null) {
            self::$iconService = new IconService();
        }

        return self::$iconService;
    }

    public function find($sno){
        $iconModel = new IconModel();
        return $iconModel->find($sno);
    }

    public function update($icon, $new_post_data)
    {
        $icon->fill($new_post_data);
        $iconModel = new IconModel();
        $isSuccess = $iconModel->save($icon);
        return [$isSuccess, $iconModel->errors()];
    }

    public function delete($sno)
    {        
        $icon = $this->find($sno);

        if (!$icon) {
            return false;
        }

        $iconModel = new IconModel();
        $iconModel->delete($icon->sno);

        return true;
    }

}