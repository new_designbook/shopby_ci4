<?php

namespace App\Services;

use App\Entities\ProductEntities;
use App\Models\ProductModel;

class ProductService
{
    private static $productService = null;

    public static function factory()
    {
        if (self::$productService === null) {
            self::$productService = new ProductService();
        }

        return self::$productService;
    }

    /**
     *  [관리자] 리스트
     */
    public function product_list($page, $perPage, $orderby)
    {
        $systemKey = $_ENV['app.SystemKey'];
        $mallKey = $_ENV['app.Outkey'];
        $param = [
            'page' => $page,
            'size' => $perPage,
            'order' => [
                'direction' => 'RECENT_PRODUCT',
                'by' => $orderby
            ]
        ];

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://server-api.e-ncp.com/products/search'.'?'.http_build_query($param, '', ),
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$_ENV['app.Token'],
			'Content-Type: application/json',
            'systemkey: '.$systemKey,
            'mallKey:'.$mallKey,
            'version: 1.0'
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		if ($err) {
			echo 'cURL Error #:' . $err;
		} else {

			$productList = json_decode($response, true);
            //echo print_r($productList,true);
            return $productList;
        }
    }

}