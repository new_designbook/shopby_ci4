<?php

namespace App\Services;

use App\Models\MovieModel;
use CodeIgniter\Config\Services;

class MovieService extends Services
{
    private static $movieService = null;
    //객체생성
    public static function factory()
    {
        if (self::$movieService === null) {
            self::$movieService = new MovieService();
        }

        return self::$movieService;
    }

    //상세
    public function find($sno){
        $movieModel = new MovieModel();
        return $movieModel->find($sno);
    }
    //수정
    public function update($arrData, $new_post_data)
    {
        $arrData->fill($new_post_data);
        $movieModel = new MovieModel();
        $isSuccess = $movieModel->save($arrData);
        return [$isSuccess, $movieModel->errors()];
    }
}