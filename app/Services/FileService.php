<?php

namespace App\Services;

use App\Entities\ProposeEntities;
use App\Models\FileModel;

class FileService
{
    private static $fileService = null;

    public static function factory()
    {
        if (self::$fileService === null) {
            self::$fileService = new FileService();
        }

        return self::$fileService;
    }

    /**
     *  파일저장 로직
     */
    protected function saveFile($file, $fieldName, $tableName)
    {
        $fileModel = new FileModel();
        if($file->getError() === 0) { // 파일이 첨부 되었을때
            $newName = $file->getRandomName();
            // FCPATH -> public 경로
            // APPPATH -> 
            $uplodePath = FCPATH.'uploads';
            $file->move($uplodePath, $newName);

            $saveData = [];
            $saveData['tableName'] = $tableName;
            $saveData['fieldName'] = $fieldName;
            $saveData['originalName'] = $file->getClientName();
            $saveData['originalMimeType'] = $file->getClientMimeType();
            $saveData['size'] = $file->getSize();
            $saveData['pathName'] = $uplodePath;
            $saveData['name'] = $newName;
            return $fileModel->insert($saveData);
        }
        return null;
    }
    /**
     *  파일 저장
     */
    public function create($files, $tableName)
    {
        $result = [];
        foreach($files as $name => $file) {
            if(is_array($file)) { // 배열인 경우
                foreach($file as $k => $f) {
                    $result[] = $this->saveFile($f, $name, $tableName);
                }
            } else { // 일반일 경우
                $result[] = $this->saveFile($file, $name, $tableName);
            }
        }
		$resultFileSno = array_filter($result);
        return implode(',', $resultFileSno);
    }
    /**
     *  파일 불러오기
     */
    public function fileInfo($snoStr)
    {
        $fileModel = new FileModel();
        $files = $fileModel->whereIn('sno', explode(',', $snoStr))->findAll();
        $result['src'] = [];
        foreach($files as $key => $file) {
            // $result[$file['fieldName']][] = $file['pathName']."\\".$file['name'];
            $result['src'][$file['fieldName']][$file['sno']] = $file['name'];
            $result['imageFl'][$file['fieldName']][$file['sno']] = (substr($file['originalMimeType'], 0, 5) === 'image') ? 'y' : 'n';
            $result['name'][$file['fieldName']][$file['sno']] = $file['originalName'];
        }
        return $result;
    }

    public function find($sno){
        $fileModel = new FileModel();
        return $fileModel->find($sno);
    }


    public function update($propose, $new_post_data)
    {
        $propose->fill($new_post_data);
        $proposeModel = new ProposeModel();
        $isSuccess = $proposeModel->save($propose);
        return [$isSuccess, $proposeModel->errors()];
    }

    public function delete($sno)
    {        
        $propose = $this->find($sno);

        if (!$propose) {
            return false;
        }

        $proposeModel = new ProposeModel();
        $proposeModel->delete($propose->sno);

        return true;
    }

    /**
     *  [관리자] 리스트
     */
    public function propose_list($page, $kind, $perPage, $orderby)
    {
        $model = new ProposeModel();
        $propose_query =  $model->orderBy("created_at", $orderby);

        $propose_list = $model->where('kind', $kind)->paginate($perPage, 'propose', $page);

        $pager = $model->pager;
        
        return [$pager, $propose_list];
    }

}